import csv
placesFile = 'places.csv'
tablesFile = 'tables.csv'

def parse_row(row):
    order = row[0]
    table_id = row[2]
    places = row[3]
    return order, table_id, places
def new_row(order, table, place):
    out = []
    out.append(order)
    out.append(table)
    out.append(place)
    return out

def tables_list(tablesFile):
    tablesList = []
    with open(tablesFile, 'r') as file:
        reader = csv.reader(file, delimiter=':')
        for row in reader:
            tablesList.append(row)
    file.close()
    return tablesList

def places_list(placesFile):
    placesList = []
    with open(placesFile) as f:
        reader = csv.reader(f, delimiter=':')
        for row in reader:
            orderId, tableId, places = parse_row(row)
            for place in places.split(','):
                placesList.append(new_row(orderId, tableId, place))
    return placesList

#print(tables_list(tablesFile))
#print(places_list(placesFile))

