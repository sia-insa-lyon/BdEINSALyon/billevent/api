from django.contrib.admin import AdminSite, ModelAdmin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.permissions import SAFE_METHODS
from import_export.resources import ModelResource
from api import models as api_models, models
from api.models import Membership
from mercanet import models as mercanet_models
from api.admin import BilletAdmin, OrderAdmin, InvitationAdmin, PlaceAdmin, TableAdmin, CompostageAdmin, \
    ClientAdmin
from import_export.admin import ImportExportModelAdmin
from import_export.fields import Field


class BilleventAdmin(AdminSite):
    site_header = "Interface Orga de Billevent"
    site_title = "Billevent Orga"
    index_title = "Bienvenue sur l'interface orga de la billetterie du BdE INSA Lyon"
    site_url = "/orga/"
    index_template = 'orga_index.html'

    def has_permission(self, request):
        if request.user.is_authenticated:
            if request.user.is_staff and request.user.is_superuser:  # autorise l'accès au superuser par défaut
                return True
            try:
                if request.user.membership and (  # autorise l'accès en lecture seule aux non-superuser
                        (
                                request.user.membership.permission_level <= Membership.LEVEL_VIEWER
                                and request.method in SAFE_METHODS
                        )
                        or request.user.membership.permission_level == Membership.LEVEL_ADMIN
                ):
                    return True
            except Membership.DoesNotExist:
                return False
            print("chelou")
        else:
            return False


admin = BilleventAdmin(name='orga_admin')

admin.register(api_models.Organizer)
admin.register(api_models.Membership)
admin.register(api_models.PricingRule)
admin.register(api_models.Product)
admin.register(api_models.Option)
admin.register(api_models.Coupon)
admin.register(api_models.InvitationGrant)
admin.register(api_models.Invitation, InvitationAdmin)
admin.register(api_models.Question)
admin.register(api_models.Categorie)
admin.register(api_models.Place, PlaceAdmin)
admin.register(api_models.Table, TableAdmin)
admin.register(api_models.Billet, BilletAdmin)
admin.register(api_models.Order, OrderAdmin)
admin.register(api_models.Compostage, CompostageAdmin)
admin.register(User, UserAdmin)


class TransactionAdmin(ModelAdmin):
    list_display = ['id', 'responseText', 'transactionReference', 'amount', 'transactionDateTime']
    list_filter = ['responseText', 'transactionDateTime']
    search_fields = ['transactionReference', 'maskedPan']
    exclude = ['customerIpAddress', 'customerMobilePhone', 'cardProductCode', 'issuerCode']
    readonly_fields = ['status']


admin.register(mercanet_models.TransactionMercanet, TransactionAdmin)


class TransactionRequestAdmin(ModelAdmin):
    def view_on_site(self, obj):
        if obj.mercanet:
            return "../../../transactionmercanet/" + str(obj.mercanet.id) + "/change/"
        # return reverse("admin:mercanet_transactionmercanet_list")
        # return reverse("admin:mercanet_transactionmercanet_"+str(obj.mercanet.id)+"_change")#, kwargs={'id': obj.mercanet})


admin.register(mercanet_models.TransactionRequest, TransactionRequestAdmin)


class ParticipantResource(ModelResource):
    order_status = Field()

    class Meta:
        model = models.Participant
        exclude = []
        fields = ['id', 'first_name', 'last_name', 'email', 'phone', 'billet', 'order_status']

    def dehydrate_order_status(self, participant):
        return participant.order_status()


class ParticipantAdmin(ImportExportModelAdmin):
    resource_class = ParticipantResource
    search_fields = ['id', 'first_name', 'last_name', 'email']
    list_per_page = 20

    def view_on_site(self, obj):
        return reverse('tree', kwargs={'participant_id': obj.id})

    list_display = ['id', "first_name", "last_name", "email", "phone", "billet", "order_status"]


admin.register(api_models.Participant, ParticipantAdmin)
admin.register(api_models.Client, ClientAdmin)


class EventAdmin(ModelAdmin):
    def view_on_site(self, obj):
        return '/orga/caisse/{}'.format(obj.id)


admin.register(api_models.Event, EventAdmin)

class FileAdmin(ModelAdmin):
    def view_on_site(self, file):
        return f'/orga/scan_billet/{file.id}'

    list_display = ['id', 'nom', 'event', 'number_scanned']

admin.register(api_models.File, FileAdmin)
# class ParticipantsAdmin(ModelAdmin):
#    def view_link(self, arg):
#        return arg
#    list_display = ['view_link']
# admin.register(api_models.Participant, ParticipantsAdmin)

# class TransactionRequestAdmin(ModelAdmin):
#    readonly_fields = ['status_de_la_transaction']
# admin.register(mercanet_models.TransactionRequest, TransactionRequestAdmin)
