import csv
placesInFile = 'places_in.csv'
tablesFile = 'tables.csv'
placesOutFile = 'places_out.csv'
tables = []
tables_dict = {}
with open(tablesFile, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    for row in reader:
        if row != ['id', 'nom_table']:
            tables.append(row) #id, nom_table
tables_id = [r[0] for r in tables]
tables_noms = [r[1].rstrip().lstrip() for r in tables]

for t in tables:
    tables_dict[t[1].rstrip().lstrip()]=int(t[0])

#for i in range(0, len(tables)):
#    print("{} -> {} : {}".format(tables[i],tables_id[i], tables_noms[i]))

places_raw = []
with open(placesInFile, 'r') as f:
    for row in csv.reader(f, delimiter=','):
        if row[0] != 'Commande':
            places_raw.append(row)
places_cleaner = [[r[0], r[1].rstrip().lstrip(), r[2].split(',')] for r in places_raw]
#for p in places_cleaner: print(p)
for i in range(0, len(places_cleaner)):
    if places_cleaner[i][0] != '':#re-écrit le numéro de la commande pour les places sur plusieurs commandes
        places_cleaner[i][0] = int(places_cleaner[i][0])
    else:
        places_cleaner[i][0] = places_cleaner[i-1][0]
    #nom_table = places_cleaner[i][1]
    #table_index = tables_noms.index(nom_table.lstrip().rstrip())
    #id_table = tables_id[table_index]
    places_cleaner[i][1] = tables_dict[places_cleaner[i][1]]
places_final = []
for places in places_cleaner:
    for p in places[2]:  # chaque place
        row = [ places[0], p, places[1]]
        places_final.append(row)

with open(placesOutFile, 'w') as f:
    writer = csv.writer(f, delimiter=',')
    writer.writerow(['order', 'place', 'table', 'id'])
    writer.writerows(places_final)
f.close()
#ensuite pour django-import-export je rajoute un champ id au pif (incrémenté)