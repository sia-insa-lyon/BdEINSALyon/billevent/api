from django.conf.urls import url

from orga import admin
from orga import views

urlpatterns = [
    url(r'^tree/(?P<participant_id>[0-9]+)', views.client_order_billet_tree, name='tree'),
    url(r'^caisse/(?P<event_id>[0-9]+)', views.feuille_caisse),
    url(r'^participants_restants/(?P<event_id>[0-9]+)', views.participants_a_composter, name='participants-compostage-restants'),
    url(r'^problemes', views.list_inconsistencies, name='problemes'),
    url(r'^recap/(?P<client_id>[0-9]+)', views.recap, name='recap'),
    url(r'^billet_summary/(?P<event_id>[0-9]+)', views.billet_summary, name='compta-summary'),
    url(r'^scan_billet/(?P<file_id>[0-9]+)', views.scan_billet, name='scan-billet'),
    url(r'^scan_check/(?P<file_id>[0-9]+)/(?P<uid>.+)', views.scan_check, name='scan-check'),
    url(r'^', admin.admin.urls),  # l'admin custom pour le Gala
]
