## POUR CREER LA DB DES TABLES
from api.models import *
from api.injectCSV import *
for t in tables_list('api/tables.csv'):
    table = Table(nom_table=t[1], id=t[0])
    table.save()

# CREATION DE FAUSSES COMMANDES
commandes = []
for c in places_list('api/places.csv'):
   commandes.append(c[0])
#pour créer des commandes avec les ids stockjés dans une liste
for c in set(commandes):
    o = Order(id=c, event_id=1)
    o.save()

#CREATION DE FAUX BILLET (qui font de vrai faussaires)
for c in set(commandes):
    b = Billet(order_id=c[0])
    b.save()
## POUR CREER LA DB DES PLACES
i=0
for p in places_list('api/places.csv'):
    if Order.objects.filter(id=p[0]).count()<1:
        client = Client.objects.get(first_name=p[0])
        Order(id=p[0], event_id=1, client=client).save()
    order = Order.objects.get(id=p[0])
    table = Table.objects.get(id=p[1])
    place = Place(order=order, table=table, place=p[2], id=i).save()
    Billet(order=order).save()
    i+=1
i=0
for p in places_list('api/places.csv'):
    order = Order.objects.get(id=p[0])
    table = Table.objects.get(id=p[1])
    place = Place(order=order, table=table, place=p[2], id=i)
    place.save()
    i+=1
# POUR SUPPRIMER LES DOUBLONS
dupes = []
for table in Table.objects.all():
    for place in Place.objects.filter(table=table).all():
        if Place.objects.filter(table=table, place=place.place).count() > 1:
            dupes_id=[]
            for a in Place.objects.filter(table=table, place=place.place).all():
                dupes_id.append(a.id)
            for i in range(1, len(dupes_id)):
                #Place.objects.get(id=dupes_id[i]).delete()
                print(Place.objects.get(id=dupes_id[i]).order.id)
                dupes.append(Place.objects.get(id=dupes_id[i]))
            dupes_id.clear()
for id in set(dupes):
    Place.objects.get(id=id.id).delete()

#POUR CREER LA DB DES USERS
for row in tables_list('api/places.csv'):
    Client(first_name=row[0], last_name=row[1], email=str(row[0])+'@bar.com').save()