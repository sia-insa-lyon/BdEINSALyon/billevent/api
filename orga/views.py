from django.contrib.auth.decorators import permission_required
from django.core.signing import TimestampSigner, Signer, BadSignature
from django.db.models import Count, Sum
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse

from api.models import Participant, Order, Billet, Product, Option, Event, Compostage, BilletOption, Question, File
from mercanet.models import TransactionMercanet
from orga.permissions import IsAuthorizedOrga


@permission_required([IsAuthorizedOrga])
def client_order_billet_tree(request, participant_id):
    participant = Participant.objects.get(pk=participant_id)
    billet = participant.billet
    commande = billet.order
    client = commande.client

    return render(request, template_name='tree.html', context={'recherche': participant,
                                                               'client': client,
                                                               'commande': commande,
                                                               'billets': commande.billets.all(),
                                                               'participants': billet.participants.all(),
                                                               'billet_link': reverse('ticket-print', kwargs={
                                                                   'id': TimestampSigner().sign(commande.id)})})


@permission_required([IsAuthorizedOrga])
def list_inconsistencies(request):
    problemes = Order.objects.filter(status__gte=Order.STATUS_PAYMENT).exclude(transaction__mercanet__responseCode="00")
    # problemes = Order.objects.exclude(pk__gt=0)
    # problemes = Order.objects.all()
    return render(request, template_name='problemes.html', context={'problemes': problemes.all()})


@permission_required([IsAuthorizedOrga])
def recap(request, commande_id=None, client_id=None, billet_id=None, participant_id=None):
    if commande_id == None and client_id == None and billet_id == None and participant_id == None:
        return
    if commande_id is not None:
        commande = Order.objects.get(pk=commande_id)
    if billet_id is not None:
        commande = Billet.objects.get(pk=billet_id).order
    if participant_id is not None:
        commande = Participant.objects.get(pk=participant_id).billet.order
    if client_id is not None:
        commandes = Order.objects.filter(client__id=client_id)
        if len(commandes) < 1:
            return HttpResponse("Il n'y a pas d'informations disponibles pour cet élément")
    else:
        commandes = [commande]
    return render(request, template_name='recap.html', context={'commandes': commandes.all()})


@permission_required([IsAuthorizedOrga])
def feuille_caisse(request, event_id):
    produits = Product.objects.filter(event=event_id)
    options = Option.objects.filter(event=event_id)

    event = Event.objects.get(id=event_id)
    compostes = Compostage.objects.filter(billet__order__event=event)
    billets = Billet.validated().filter(order__event=event, order__status=Order.STATUS_VALIDATED)
    billets_compostes = billets.filter(compostage__in=compostes)
    produits_compostes = billets_compostes.values('product').annotate(total=Count('product'))
    comptes_produit = []
    dict_produits_compostes = {a['product']: a['total'] for a in produits_compostes}
    final_produits = []
    for prd in produits:
        if prd.id in dict_produits_compostes.keys():
            prd.__setattr__('compostes', dict_produits_compostes[prd.id])
        else:
            prd.__setattr__('compostes', 0)
        prd.__setattr__('restants',
                        prd.reserved_units() - prd.compostes)  # restants inclut dans ses comptes les billets pas encore payés (dt<20min)
        final_produits.append(prd)

    options_compostes = BilletOption.objects.filter(option__event=event_id, billet__compostage__in=compostes,
                                                    billet__canceled=False) \
        .values('option').annotate(total=Count('option'))
    dict_options_compostes = {o['option']: o['total'] for o in options_compostes}
    final_options = []
    for opt in options:
        if opt.id in dict_options_compostes.keys():
            opt.__setattr__('compostes', dict_options_compostes[opt.id])
        else:
            opt.__setattr__('compostes', 0)
        opt.__setattr__('restants', opt.reserved_units() - opt.compostes)
        final_options.append(opt)

    mercanets = \
    TransactionMercanet.objects.filter(request__order__event=event, responseCode="00").aggregate(total=Sum('amount'))[
        'total'] / 100

    total_commandes = 0
    for order in Order.objects.filter(event=event, status=Order.STATUS_VALIDATED):
        total_commandes += order.amount

    return render(request, template_name='caisse.html', context={
        'event': event,
        'produits': produits,
        'options': options,
        'compostes': compostes.count(),
        'total': billets.count(),
        'restants': billets.count() - compostes.count(),
        'comptes': comptes_produit,
        'final_option': final_options,
        'final_produit': final_produits,
        'expected_mercanet': mercanets,
        'excpected_orders': total_commandes
    })


@permission_required([IsAuthorizedOrga])
def participants_a_composter(request, event_id):
    event = Event.objects.get(id=event_id)
    compostes = Compostage.objects.filter(billet__order__event=event)
    billets_restants = Billet.validated().filter(order__event=event).exclude(compostage__in=compostes)
    parts = Participant.objects.filter(billet__order__event=event).exclude(billet__compostage__in=compostes)
    return render(request, 'participants_restants.html', context={
        'billets_restants': billets_restants,
        'participants': parts,
    })


@permission_required([IsAuthorizedOrga])
def billet_summary(request, event_id):
    page = int(request.GET['page'])
    howmany = int(request.GET['by'])
    quest = request.GET['questions']
    status = request.GET['status']

    event = Event.objects.get(id=event_id)
    billets = Billet.objects.all().filter(order__event=event).order_by('id')
    if status == "oui":
        billets = billets.filter(order__status=7)
    billets = billets[page * howmany:(page + 1) * howmany]

    questions = Question.objects.all()

    return render(request, template_name='billet_summary.html', context={
        'billets': billets,
        'event': event,
        'questions': questions,
        'page': page,
        'howmany': howmany,
        'quest': quest
    })


@permission_required([IsAuthorizedOrga])
def scan_billet(request, file_id):
    return render(request, template_name='scan_billet.html', context={'file': File.objects.get(pk=file_id)})


@permission_required([IsAuthorizedOrga])
def scan_check(request, file_id, uid):
    try:
        id = Signer().unsign(uid.strip())
        file = File.objects.get(pk=file_id)
        try:
            if Billet.objects.get(id=id).order.status == Order.STATUS_VALIDATED and not Billet.objects.get(
                    id=id).canceled and not Billet.objects.get(id=id).refunded:
                billet = Billet.objects.get(id=id)
                if Compostage.objects.filter(billet=billet).count() >= 1:
                    scanned = True
                    compostage = Compostage.objects.filter(billet=billet).first()
                else:
                    scanned = False
                    compostage = Compostage.objects.create(billet=billet, file=file)
                return render(request, template_name='scan_check.html',
                              context={'billet': billet, 'scanned': scanned, 'compostage': compostage, 'file': file, 'exist': True})
            else:
                return HttpResponse("Billet invalide", status=401)
        except Billet.DoesNotExist:
            return render(request, template_name='scan_check.html', context={'file': file, 'exist': False})
    except BadSignature:
        return HttpResponse("Erreur de lecture", status=406)
