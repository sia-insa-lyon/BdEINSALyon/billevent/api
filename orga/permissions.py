from rest_framework.permissions import BasePermission, SAFE_METHODS

from api.models import Membership


class IsOrgaSoft(object):
    def has_permission(self, request, view):
        try:
            return request.user.membership.permission_level <= Membership.LEVEL_VIEWER \
             and request.method in SAFE_METHODS and request.user.membership
        except Membership.DoesNotExist: return False

class IsAuthorizedOrga(IsOrgaSoft):
    def has_permission(self, request, view):
        try:
            return super().has_permission(request, view) or \
                      (request.user.membership.permission_level == Membership.LEVEL_ADMIN and request.user.membership) \

        except Membership.DoesNotExist: return False

class IsRespOrga(BasePermission):
    def has_permission(self, request, view):
        try:
            return (request.user.membership.permission_level == Membership.LEVEL_ADMIN
                    and request.user.membership)
        except Membership.DoesNotExist:
            return False