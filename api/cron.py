from typing import Iterable
from api.models import Order
from mercanet.models import TransactionRequest


def clean_old_orders():
    """
    NOTE: ça peut toujours supprimmer la commande de quelqu'un qui a attendu super longtemps pour cliquer sur le bouton "payer", mais au moins
    ça lui fera une erreur s'il essaie au lieu de payer pour rien
    """
    unpaid_orders:Iterable[Order] = Order.objects.exclude(id__in=Order.accountable_orders())
    deleted=[]
    for order in unpaid_orders:
        if order.transaction is not None:
            if not (order.transaction.status == TransactionRequest.STATUSES['PAYED'] or order.transaction.status == TransactionRequest.STATUSES['PAYING']):
                deleted.append(order.delete())
        else:
            deleted.append(order.delete())
    return deleted
