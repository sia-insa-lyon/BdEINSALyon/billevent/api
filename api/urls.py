from django.conf.urls import url, include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from rest_framework_nested import routers

from . import views
from . import views_admin as admin

router = routers.SimpleRouter()
router.register(r'events', views.EventsViewSet)
router.register(r'products', views.ProductViewSet, 'products')
router.register(r'options', views.OptionViewSet, 'options')
router.register(r'billets', views.BilletViewSet, 'billet')
router.register(r'order', views.OrderViewSet, 'orders')
router.register(r'transactions', views.TransactionViewSet, 'transactions')
router.register(r'compostages', views.CompostageViewSet, 'compostages')
router.register(r'files', views.FileViewSet)
router.register(r'client', views.ClientViewSet, 'client')  # espace client
router.register(r'clientraid', views.ClientRAIDViewSet, 'clientraid')  # API dossieraid, pour créer des clients
router.register(r'invitation', views.InvitationRAID, 'invitation')  # API dossieraid, pour créer des invitations
router.register(r'events-products', views.ProductsByEvent, 'event-product')  # future implémentation dans dossieraid
router.register(r'grants', views.GrantViewSet, 'grants')  # pour dossieraid
router.register(r'get_order', views.APIOrderViewSet, 'get_order')  # pour Dossieraid, utilise des ID
router.register(r'participant', views.ParticipantImageUpload, 'participant')
# router.register(r'user', views.CreateUser, 'user')

admin_router = routers.SimpleRouter()
admin_router.register(r'events', admin.EventViewSet, 'events')
admin_router.register(r'organizers', admin.OrganizerViewSet, 'organizers')
admin_router.register(r'invitations', admin.InvitationViewSet, 'invitations')
admin_router.register(r'billets', admin.BilletsViewSet, 'billets-admin')
admin_router.register(r'orders', admin.OrdersViewSet, 'orders-admin')
admin_router.register(r'questions', admin.QuestionsViewSet, 'question-admin')
admin_router.register(r'answers', admin.AnswersViewSet, 'answer-admin')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^billetcheck/', views.billet_check, name="billet-check"),
    url(r'^authenticate/invitation$', views.InvitationAuthentication.as_view()),
    url(r'^authenticate$', obtain_jwt_token),
    url(r'^authenticate/refresh$', refresh_jwt_token),
    url(r'^authenticate/logout$', views.LogoutViews.as_view()),
    url(r'^', include(router.urls)),
    url(r'^admin/', include(admin_router.urls)),
    url(r'^rules', views.RulesViews.as_view()),
    url(r'^me', views.CurrentUserViews.as_view()),
    url(r'place/(?P<id>[0-9]+)', views.PlaceView.as_view()),
    url(r'search/(?P<search>.+)', views.search),
    url(r'invitation/(?P<pk>[0-9]+)/send_mail/$', views.send_mail),
    url(r'^', include('rest_framework.urls', namespace='rest_framework')),
]
