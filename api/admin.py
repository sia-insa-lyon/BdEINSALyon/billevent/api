from django.conf import settings
from django.contrib import admin
from django.core.signing import TimestampSigner
from django.db.models import Q, Sum
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from import_export.admin import ImportExportModelAdmin, ImportExportActionModelAdmin
from import_export.resources import ModelResource
from import_export.fields import Field

from . import models


@admin.register(
    models.Organizer,
    models.Membership,
    models.PricingRule,
    models.Product,
    models.Option,
    models.Coupon,
    models.InvitationGrant,
    models.PaymentMethod,
    models.Question,
    models.Categorie,
    models.Transaction,
    models.File,
    models.VA,
    models.Stand,
)
class BasicAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Permanence)
class PermanenceAdmin(admin.ModelAdmin):
    list_display = ('stand', 'permanencier', 'created_at', 'closed_at', 'event')
    list_filter = ('stand', 'permanencier', 'created_at', 'closed_at', 'event')


@admin.register(models.BilletOption)
class BilletOptionAdmin(admin.ModelAdmin):
    list_display = ['id', 'participant', 'billet', 'option', 'amount']
    search_fields = ['participant__first_name', 'participant__last_name',
                     'billet__order__client__first_name', 'billet__order__client__last_name', 'billet__id',
                     'billet__order__id']
    raw_id_fields = ("participant", 'billet', 'option')
    fields = ("participant", 'billet', 'option', 'amount')
    list_filter = ['billet__order__event__name', 'option__name']
    list_per_page = 20

    def get_queryset(self, request):
        qs = super(BilletOptionAdmin, self).get_queryset(request)
        return qs.filter(Q(billet__order__id=None) | Q(billet__order__status=models.Order.STATUS_VALIDATED))


@admin.register(models.Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ['id', 'order', 'participant', 'billet', 'question']
    search_fields = ['participant__first_name', 'participant__last_name',
                     'order__client__first_name', 'order__client__last_name', 'billet__id', 'order__id']
    raw_id_fields = ("participant", "order", 'billet', 'question')
    fields = ("participant", "order", 'billet', 'question', 'value')
    list_filter = ['order__event__name', 'question__question']
    list_per_page = 20

    def get_queryset(self, request):
        qs = super(AnswerAdmin, self).get_queryset(request)
        return qs.filter(Q(order__id=None) | Q(order__status=models.Order.STATUS_VALIDATED))


@admin.register(models.Client)
class ClientAdmin(admin.ModelAdmin):
    search_fields = ['first_name', 'last_name', 'email']
    list_per_page = 20

    def view_on_site(self, obj):
        return reverse('recap', kwargs={'client_id': obj.id})


class ParticipantInline(admin.StackedInline):
    model = models.Participant

    def get_max_num(self, request, obj=None, **kwargs):
        if obj is None:
            return None
        if obj.product is None:
            return 0
        return obj.product.seats


class BilletOptionInline(admin.StackedInline):
    model = models.BilletOption
    raw_id_fields = ("option", "participant")
    extra = 0

    def lien_billet(billet: models.Billet):
        return format_html('<a href="/admin/api/billet/{bid}/">ouvrir</a>', bid=billet.id)


class BilletResource(ModelResource):
    product = Field()
    participants = Field()
    options = Field()
    rembourse = Field()
    options = Field()
    annule = Field()
    mercanet = Field()
    amount = Field()
    date = Field()
    anwsers = Field()

    class Meta:
        model = models.Billet
        fields = ('id', 'product', 'participants', 'options', 'rembourse', 'annule', 'mercanet', 'amount', 'date', "anwsers")

    def dehydrate_product(self, billet):
        try:
            if billet.product:
                return billet.product.name
        except Exception:
            return ""

    def dehydrate_participants(self, billet):
        try:
            p = []
            for participant in billet.participants.all():
                p.append(str(participant))
            return ' # '.join(p)
        except Exception:
            return ""

    def dehydrate_options(self, billet):
        try:
            options = []
            if billet.options:
                for option in billet.options.all():
                    options.append(option.name)

            return ";".join(options)
        except Exception:
            return ""

    def dehydrate_rembourse(self, billet):
        try:
            if billet.refunded:
                return "Oui"
            else:
                return "Non"
        except Exception:
            return ""

    def dehydrate_annule(self, billet):
        try:
            if billet.canceled:
                return "Oui"
            else:
                return "Non"
        except Exception:
            return ""

    def dehydrate_mercanet(self, billet):
        try:
            if billet.order and billet.order.transaction and billet.order.transaction.mercanet:
                return billet.order.transaction.mercanet.transactionReference
            return ''
        except Exception:
            return ""

    def dehydrate_amount(self, billet):
        try:
            if billet.order.transaction:
                return billet.order.transaction.amount/100 # Conversion en centimes
        except Exception:
            return ""

    def dehydrate_date(self, billet):
        try:
            if billet.order.transaction and billet.order.transaction.mercanet.transactionDateTime:
                return billet.order.transaction.mercanet.transactionDateTime.strftime('%m/%d/%Y %I:%M')
        except Exception:
            return ""

    def dehydrate_anwsers(self, billet):
        try:
            answers = []
            if billet.order.answers:
                for ans in billet.order.answers.all():
                    if ans.question and ans.value:
                        answers.append(f"#{ans.question.id} : {ans.value}")
            return "\n".join(answers)
        except Exception:
            return ""


@admin.register(models.Billet)
class BilletAdmin(ImportExportActionModelAdmin):
    resource_class = BilletResource
    list_display = ['id', 'order', 'participants', 'mercanet', 'product']
    search_fields = ['participants__first_name', 'participants__last_name', 'participants__email']
    raw_id_fields = ('order', 'product')
    list_filter = ['canceled', 'refunded', 'order__event__name', 'order__status', 'product__name']
    inlines = (BilletOptionInline, ParticipantInline)
    list_per_page = 100

    actions = ImportExportActionModelAdmin.actions + ['autoriser_liste_attente']

    def participants(self, billet):
        p = []
        for participant in billet.participants.all():
            p.append(str(participant))
        return ' # '.join(p)

    def mercanet(self, billet):
        if billet.order.transaction and billet.order.transaction.mercanet:
            return billet.order.transaction.mercanet.transactionReference

        return ''

    def autoriser_liste_attente(self, request, queryset):
        """
        Permet de d'automatiser le processus pour donner une autorisation d'achat à un client sur liste d'attente pour
        qu'il achète un des produits ré-équilibrage et accès par exemple au Repas en ayant acheté un billet
        Repas + liste d'attente Soirée
        """
        created = 0
        for b in queryset:
            billet: models.Billet = b
            if billet.product.liste_attente_gala.count() > 0:  # si c'est un produit qui donne accès à une liste d'attente
                if billet.order.client.invitations.count() > 0:  # il faut qu'il y ait une invitation
                    invitation = billet.order.client.invitations.last()
                    for needs_grant_prods in billet.product.liste_attente_gala.filter(selling_mode='I'):
                        already_allowed_for_this_product = models.InvitationGrant.objects.filter( # autorisations pour ce produit accordées par l'orga
                            product=needs_grant_prods,
                            invitation=invitation
                        ).aggregate(total=Sum('amount')).get('total', None)
                        if already_allowed_for_this_product is None: already_allowed_for_this_product = 0
                        reserved_places = models.Billet.objects.filter(order__client=invitation.client, # places que le client a acheté
                                                                                        product=billet.product).count() # qui donnent accès à des produits en liste d'attente
                        if already_allowed_for_this_product < reserved_places:
                            # si le client a acheté plus de places de type liste d'attente que le nombre qui lui ont déjà été attribuées
                            # on lui autorise encore un ré-équilibrage
                            ig = models.InvitationGrant(invitation=invitation, product=needs_grant_prods, amount=1)
                            ig.save()
                            created += 1
        self.message_user(request, "{} Autorisations d'Invitations crées".format(created))

    autoriser_liste_attente.short_description = "Autoriser les ré-équilibrages"


class InvitationGrantInline(admin.StackedInline):
    model = models.InvitationGrant
    extra = 3


class InvitationResource(ModelResource):
    class Meta:
        model = models.Invitation
        exclude = ('client', 'link_sent')


@admin.register(models.Invitation)
class InvitationAdmin(ImportExportModelAdmin):
    search_fields = ['first_name', 'last_name', 'email']
    raw_id_fields = ('client',)
    inlines = (InvitationGrantInline,)
    list_per_page = 20
    actions = ['send_mail', 'send_new_mail']
    resource_class = InvitationResource

    def send_mail(self, request, queryset):
        i = 0
        for invitation in queryset:
            invitation.send_email()
            invitation.save()
            i += 1
        self.message_user(request, "{} mails d'invitation ont été envoyé(s)".format(i))

    def send_new_mail(self, request, queryset):
        i = 0
        for invitation in queryset.filter(link_sent=False):
            invitation.send_email()
            invitation.save()
            i += 1
        self.message_user(request, "{} mails d'invitation ont été envoyé(s)".format(i))

    send_mail.short_description = "Renvoyer les mails d'invitation"
    send_new_mail.short_description = "Envoyer les mails d'invitation"

    def view_on_site(self, obj: models.Invitation):
        # "{}/invitation/{}".format(settings.BILLEVENT['FRONTEND_URL'], self.invitation.token)
        return f"{settings.BILLEVENT['FRONTEND_URL']}/invitation/{obj.token}"



class BilletInline(admin.StackedInline):
    model = models.Billet
    extra = 0

    def admin_link(self, instance):
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label,
                                              instance._meta.module_name),
                      args=(instance.id,))
        return format_html(u'<a href="{}">Edit: {}</a>', url, instance.title)

    def participants(self, billet):
        p = []
        for participant in billet.participants.all():
            p.append(str(participant))
        return ' # '.join(p)

    def options_by(self, billet):
        p = []
        for o in billet.billet_options.all():
            p.append('{} x{}'.format(str(o.option.name), o.amount))
        return ' # '.join(p)

    options_by.verbose_name = 'Options'

    readonly_fields = ('participants', 'options_by')


class AnswerInline(admin.StackedInline):
    model = models.Answer
    raw_id_fields = ("participant", "order", 'billet', 'question')
    extra = 0


@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'event', 'client', 'status', 'mercanet', 'created_at', 'amount']
    search_fields = ['client__first_name', 'client__last_name', 'client__email',
                     'transaction__mercanet__transactionReference']
    list_display_links = ['id']
    list_select_related = ('client',)
    list_filter = ['event__name', 'status']
    list_per_page = 20

    raw_id_fields = ("transaction", "client", 'coupon')
    fields = ('event', 'client', 'status', 'transaction', 'coupon')
    inlines = (BilletInline, AnswerInline)

    actions = ['send_tickets']

    def send_tickets(self, request, queryset):
        queryset = queryset.filter(status=models.Order.STATUS_VALIDATED)
        sent = 0
        for order in queryset.all():
            order.send_tickets()
            sent += 1
        if sent == 1:
            message_bit = _('1 message a  été envoyé')
        else:
            message_bit = _("%s messages ont été envoyés") % sent
        self.message_user(request, message_bit)

    send_tickets.short_description = _('Renvoyer les billets aux commandes valides')

    def billets(self, order):
        return order.billets.all()

    def mercanet(self, order):
        if order.transaction and order.transaction.mercanet:
            return order.transaction.mercanet.transactionReference
        elif order.transaction_guichet:
            return order.transaction_guichet
        return ''

    def amount(self, o):
        return "{}€".format(o.amount)

    def view_on_site(self, obj):
        return reverse('ticket-print', kwargs={'id': TimestampSigner().sign(obj.id)})


@admin.register(models.Compostage)
class CompostageAdmin(admin.ModelAdmin):
    list_display = ['id', 'billet', 'file', 'date', 'participants']
    search_fields = ['id', 'billet__id', 'file__nom', 'date', 'billet__order__client__first_name',
                     'billet__order__client__last_name']
    raw_id_fields = ['billet']
    fields = ['billet', 'file']
    list_filter = ['file__nom', 'date']
    list_per_page = 20


class TableResource(ModelResource):
    class Meta:
        model = models.Table
        exclude = []


@admin.register(models.Table)
class TableAdmin(ImportExportModelAdmin):
    resource_class = TableResource
    list_display = ('id', 'nom_table')


class PlaceResource(ModelResource):
    class Meta:
        model = models.Place
        exclude = []
        # fields = ['order', 'place', 'table']
        # import_id_fields = ['order', 'place', 'table']


@admin.register(models.Place)
class PlaceAdmin(ImportExportModelAdmin):
    """
    Recherche par client et non pas par participant
    """
    resource_class = PlaceResource
    list_display = ('participant', 'place', 'table', 'repas_particulier')
    list_filter = ('table', 'repas_particulier')
    search_fields = ('participant__first_name', 'participant__last_name')

@admin.register(models.Participant)
class ParticipantAdmin(admin.ModelAdmin):
    search_fields = ['first_name', 'last_name', 'email']
    list_per_page = 20

    def view_on_site(self, obj):
        return reverse('tree', kwargs={'participant_id': obj.id})


@admin.register(models.Event)
class EventAdmin(admin.ModelAdmin):
    def view_on_site(self, obj):
        return '/orga/caisse/{}'.format(obj.id)
