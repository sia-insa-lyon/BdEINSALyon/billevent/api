from datetime import datetime, timedelta

from django.contrib.auth.models import Group, User
from django.db import models
from django.db.models import Sum, Count
from django.db.models.signals import pre_save, post_save
from django.dispatch.dispatcher import receiver
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _

from api.email import InvitationEmail, TicketsEmail, TicketsReminderEmail
from mercanet.models import TransactionRequest, TransactionMercanet

TARGETS = (
    ('Order', _('Globalement sur la commande')),
    ('Billet', _('Pour chaque billet')),
    ('Participant', _('Pour chaque participant')),
)


class File(models.Model):
    """
    Représente un file de lecture de billet
    """
    VALIDATION_MODES = (
        (0, "Aucun mode défini"),
        (1, "Validation de produits"),
        (2, "Validation d'options"),
        (3, "Lecture numéro place")
    )

    nom = models.CharField(max_length=100)
    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    active = models.BooleanField(default=False)
    file_parente = models.ForeignKey('File', null=True, blank=True, on_delete=models.CASCADE)
    product_type = models.ManyToManyField('Product', blank=True)
    option_type = models.ManyToManyField('Option', blank=True)
    validation_type = models.IntegerField(choices=VALIDATION_MODES, default=0)

    def __str__(self):
        return "File {}".format(self.nom)

    def number_scanned(self):
        return Compostage.objects.filter(file__id=self.id).count()


class Compostage(models.Model):
    """
    Fait la liaison entre les files et les billets.
    """
    billet = models.ForeignKey("Billet", on_delete=models.CASCADE)
    file = models.ForeignKey("File", on_delete=models.CASCADE)
    date = models.DateTimeField(auto_created=True, auto_now_add=True)

    def __str__(self):
        return "Billet " + str(self.billet.id) + " validé à la file " + str(self.file.nom) + " le " + str(self.date)

    @property
    def participants(self) -> str:
        res = ""
        for p in self.billet.participants.all():
            res += "{} {}, ".format(p.first_name, p.last_name)
        return res


class Membership(models.Model):
    LEVEL_ADMIN = 0
    LEVEL_MANAGER = 100
    LEVEL_PERMANENCIER = 150
    LEVEL_VIEWER = 1000
    LEVELS = (
        (LEVEL_ADMIN, _('Administrateur')),
        (LEVEL_MANAGER, _('Gestionnaire')),
        (LEVEL_PERMANENCIER, _('Permanencier')),
        (LEVEL_VIEWER, _('Lecture seule'))
    )

    user = models.OneToOneField(User, on_delete=models.PROTECT, related_name='membership')
    organization = models.ForeignKey('Organizer', on_delete=models.PROTECT, null=True, blank=True)
    events = models.ManyToManyField('Event', blank=True)
    created_at = models.DateTimeField(auto_created=True, blank=True)
    permission_level = models.IntegerField(choices=LEVELS)

    def valid(self, level):
        return level >= self.permission_level

    class Meta:
        verbose_name_plural = "Contrôle d'accès Orgas"
        verbose_name = "Comptes Orgas"

    def __str__(self):
        return str(self.user) + "@'" + str(self.organization) + "' pour '" + ','.join(
            str(x) for x in self.events.all()) + "'"


class Organizer(models.Model):
    class Meta:
        verbose_name = _('Organisateur')

    name = models.CharField(max_length=250)
    phone = models.CharField(max_length=15, blank=True)
    address = models.CharField(max_length=250, blank=True)
    email = models.EmailField()
    users = models.ManyToManyField(User, through=Membership, related_name='organizations')

    def __str__(self):
        return self.name


class Event(models.Model):
    VISIBILITY = (('closed', 'Fermée'), ('hidden', 'Cachée'), ('invite', 'Par invitation'), ('public', 'Public'))

    class Meta:
        verbose_name = _('Evènement')

    name = models.CharField(verbose_name=_("Nom de l'évènement"), max_length=255)
    visibility = models.CharField(max_length=20, choices=VISIBILITY, default='closed', verbose_name=_('Visibilitée'))
    description = models.TextField(verbose_name=_("Description"))
    ticket_background = models.ImageField(verbose_name=_("Fond d'image tickets"), blank=True)
    ui_banner = models.URLField(verbose_name=_("Bannière billetterie"),blank=True, max_length=5000)
    sales_opening = models.DateTimeField(default=datetime.now, verbose_name=_("Ouverture des ventes"))
    sales_closing = models.DateTimeField(default=datetime.now, verbose_name=_("Clôture des ventes"))
    # Un seats est unique pour un seul participant
    max_seats = models.IntegerField(default=1600, verbose_name=_('Nombre maximal de place'))
    # Utilisé pour des objectifs de statistiques
    seats_goal = models.IntegerField(default=1600, verbose_name=_('Nombre de place visé'))
    logo_url = models.CharField(max_length=2500, verbose_name=_('Url du logo'),
                                default='http://logos.bde-insa-lyon.fr/bal/Logo_bal.png', blank=True,
                                null=True)
    organizer = models.ForeignKey(Organizer, related_name='events', blank=True, null=True, on_delete=models.CASCADE)
    # Ouverture des portes
    start_time = models.DateTimeField(verbose_name=_('Début de l\'évènement'), default=datetime.now)
    # Fermeture de l'évènement
    end_time = models.DateTimeField(verbose_name=_('Fin de l\'évènement'), default=datetime.now)
    # Site web de l'évènement
    website = models.CharField(verbose_name=_('Site Web'), max_length=250, blank=True, default="")
    # Nom de la salle
    place = models.CharField(verbose_name=_('Nom du lieu'), max_length=250, blank=True, default="")
    # Adresse de l'évènement
    address = models.CharField(verbose_name=_('Adresse du lieu'), max_length=250, blank=True, default="")
    # Template pour les mails d'invitation
    invitation_mail_template_html = models.TextField(verbose_name=_("Template HTML du mail d'invitation"), blank=True,
                                                     default="""<p>Bonjour {{ invitation.first_name }},</p><p>Vous avez été convié(e) à participer au test de la billetterie l'évènement « {{ invitation.event.name }} ». L'accès à la billetterie se fait au moyen de ce lien :</p><p style="text-align: center"><a href="{{ link }}">{{ link }}</a></p><p>En suivant ce lien, vous pourrez donc sélectionner les formules de votre choix pour vous et vos invités.</p><p> Pour toutes questions n'hésitez pas à contacter notre équipe à l'adresse suivante: {{ invitation.event.organizer.email }}.</p><p> Votre participation à ce test est très importante pour nous. Merci d'y participer.</p><p> Dans l'attente de vous retrouver,</p><p>L'équipe {{ invitation.event.name }}</p>""")
    invitation_mail_template_text = models.TextField(verbose_name=_("Template texte du mail d'invitation"), blank=True,
                                                     default="Bonjour {{ invitation.first_name }},\n\nVous avez été convié(e) à participer au test de la billetterie l'évènement « {{ invitation.event.name }} ».\nL'accès à la billetterie se fait au moyen de ce lien :\n{{ link }}\n\nEn suivant ce lien, vous pourrez donc sélectionner les formules de votre choix pour vous et vos invités.\n\nNous vous rappelons que vous avez le droit à {{ invitation.seats }} invité(s).\nUne fois les formules sélectionnées vous serez dirigé vers une plateforme de paiement sécurisée.\n\nPour toutes questions n'hésitez pas à contacter notre équipe à l'adresse suivante: {{ invitation.event.organizer.email }}.\n\nDans l'attente de vous retrouver,\n\n\n--\nL'équipe {{ invitation.event.name }}")

    tickets_mail_template_html = models.TextField(verbose_name=_("Template HTML du mail de billets"), blank=True,
                                                  default="""<p>Bonjour {{ order.client.first_name }},</p><p>Vous avez récemment acheté des billets pour l'évènement « {{ order.event.name }} ». Vous pouvez accéder à vos billets en cliquant sur ce lien :</p><p style="text-align: center"><a href="{{ link }}">{{ link }}</a></p><p>Vous aurez besoin de vos billets pour accéder à l'évènementle jour J.Nous vous conseillons de les imprimer.</p><p> Pour toutes questions n'hésitez pas à contacter notre équipe à l'adresse suivante: {{ order.event.organizer.email }}.</p><p> Dans l'attente de vous retrouver,</p><p>L'équipe {{ order.event.name }}</p>""")
    tickets_mail_template_text = models.TextField(verbose_name=_("Template texte du mail de billets"), blank=True,
                                                  default="Bonjour {{ order.client.first\nname }},\n\nVous avez récemment acheté des billets pour l'évènement\n« {{ order.event.name }} ».\nVous pouvez accéder à vos billets au moyen de ce lien :\n{{ link }}\n\nVous aurez besoin de vos billets pour accéder à l'évènement\nle jour J.\nNous vous conseillons de les imprimer.\n\nPour toutes questions n'hésitez pas à contacter notre équipe à\nl'adresse suivante: {{ order.event.organizer.email }}.\n\nDans l'attente de vous retrouver,\n\n--\nL'équipe {{ order.event.name }}\n")

    def __str__(self):
        return self.name

    @property
    def products(self):
        return self.product_set

    @staticmethod
    def for_user(user):
        queryset = Event.objects.filter(sales_opening__lte=timezone.now(), sales_closing__gt=timezone.now()).exclude(visibility='closed')
        try:
            client = user.client
            return queryset.filter(visibility='public') | \
                   queryset.filter(invitations__client=client)
        except Client.DoesNotExist:
            return queryset.filter(visibility='public')


class Categorie(models.Model):
    class Meta:
        verbose_name_plural = "Catégories des Produits"
        verbose_name = "Catégorie de Produit"

    name = models.CharField(max_length=50)
    desc = models.CharField(max_length=255, blank=True)
    event = models.ForeignKey(Event, verbose_name=_('Evènements'), on_delete=models.CASCADE)
    hide_on_full = models.BooleanField(default=False, verbose_name=_('Cacher si tous les produits sont indisponible'))

    def __str__(self):
        return self.name


class Pricing(models.Model):
    class Meta:
        verbose_name = _('Tarification')
        abstract = True

    name = models.CharField(max_length=255)
    seats = models.IntegerField(default=1)
    price_ht = models.DecimalField(verbose_name=_('Prix HT'), decimal_places=2, max_digits=11)
    price_ttc = models.DecimalField(verbose_name=_('Prix TTC'), decimal_places=2, max_digits=11)
    rules = models.ManyToManyField("PricingRule", blank=True)
    questions = models.ManyToManyField("Question", blank=True)
    event = models.ForeignKey(Event, verbose_name=_('Evènement'), on_delete=models.CASCADE)
    description = models.TextField(verbose_name=_('Description'), blank=True, null=True)
    selling_mode = models.CharField(max_length=1, choices=(
        ('P', _('Public (en fonction de l\'évènement)')),
        ('I', _('Sur invitation direct')),
        ('L', _('Verrouillé'))
    ), default='P', verbose_name=_('mode de vente'))

    def full_name(self):
        return '{} - {}€'.format(self.name, self.price_ttc)

    def reserved_units(self, billets=None):
        if billets is None:
            billets = Billet.validated()
        if type(self) is Product:
            return billets.filter(product=self).aggregate(total=Count('id'))['total']
        if type(self) is Option:
            return BilletOption.objects.filter(billet__in=billets, option=self, billet__refunded=False, billet__canceled=False) \
                       .aggregate(total=Sum('amount'))['total'] or 0

    def reserved_seats(self, billets=None, *args, **kwargs):
        return self.reserved_units(billets) * (self.seats or 1)

    @property
    def how_many_left(self) -> int:
        """
        :return: Le nombre de produit restant ou -1 si il en reste une infinité/quantitée indé
        """
        rules = self.rules.filter(type=PricingRule.TYPE_T)
        if len(rules) <= 0:
            return -1
        # noinspection PyUnresolvedReferences
        seats = [rule.value - Pricing.reserved_seats_for(rule.pricings) for rule in rules]
        return min(seats)

    @staticmethod
    def reserved_seats_for(pricings, billets=None):
        count = 0
        for pricing in pricings:
            count += pricing.reserved_seats(billets)
        return count

    def __str__(self):
        return self.name
    @property
    def sold_units(self):
        billets = Billet.objects.filter(order__event=self.event, order__status=Order.STATUS_VALIDATED)
        return self.reserved_units(billets)
    @property
    def expected_profit(self):
        return self.reserved_units(Billet.objects.filter(order__event=self.event, order__status=Order.STATUS_VALIDATED,
                                                         canceled=False, refunded=False)) * self.price_ttc

    @property
    def expected_income(self):
        return self.reserved_units(Billet.objects.filter(order__event=self.event, order__status=Order.STATUS_VALIDATED)) * self.price_ttc

    @property
    def expected_refunds(self):
        #return self.reserved_units(Billet.objects.filter(order__event=self.event, refunded=True)) * self.price_ttc
        return self.refunded_units() * self.price_ttc

    @property
    def cancelled_units(self):
        return self.reserved_units(Billet.objects.filter(order__event=self.event, canceled=True))

    def refunded_units(self):
        return self.reserved_units(Billet.objects.filter(order__event=self.event, refunded=True))


class Product(Pricing):
    categorie = models.ForeignKey(Categorie, default=1, verbose_name=_('Catégorie'), related_name='products',
                                  on_delete=models.CASCADE)
    liste_attente_gala = models.ManyToManyField(to='Product', verbose_name="Produits liés sur liste d'attente",
                                                blank=True)

    @property
    def type_liste_attente(self) -> bool:
        return self.liste_attente_gala.count() > 0

    class Meta:
        verbose_name = _('Tarif des produit')

    def __str__(self):
        return self.name + " - " + self.categorie.name


class Option(Pricing):
    class Meta:
        verbose_name = _('Tarif des option')

    products = models.ManyToManyField(Product, related_name='options')
    target = models.CharField(max_length=30, choices=TARGETS, default='Participant')
    type = models.CharField(max_length=30, choices=(('single', _('Unique')), ('multiple', _('Plusieurs'))))


def generate_token():
    return get_random_string(32)


class InvitationGrant(models.Model):
    class Meta:
        verbose_name_plural = "Invitations : Autorisations d'achat"
        verbose_name = "Autorisation d'achat"

    invitation = models.ForeignKey('Invitation', on_delete=models.CASCADE, related_name=_('grants'))
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name=_('produit'))
    amount = models.IntegerField(verbose_name=_('quantité'))

    def __str__(self):
        return str(self.product) + ' x' + str(self.amount)


class Invitation(models.Model):
    seats = models.IntegerField(default=1)
    email = models.EmailField()
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    link_sent = models.BooleanField(blank=True, default=False)
    reason = models.TextField(blank=True, default='')
    event = models.ForeignKey(Event, related_name='invitations', on_delete=models.CASCADE)
    client = models.ForeignKey('Client', related_name='invitations', blank=True, null=True, on_delete=models.CASCADE)
    token = models.CharField(max_length=32, default=generate_token)

    def __str__(self):
        return "#" + str(self.id) + "Invitation client " + str(self.client)

    @property
    def bought_seats(self):
        billets = Billet.validated().filter(order__client=self.client)
        count = 0
        for pricing in self.event.products.all():
            count += pricing.reserved_seats(billets)
        return count

    def send_email(self):
        email = InvitationEmail(self, to=(self.email,))
        self.link_sent = email.send(True) > 0


@receiver(pre_save, sender=Invitation)
def before_save_invitation_map_client(sender, instance, raw, **kwargs):
    if instance.client_id is None:
        instance.client, created = Client.objects.get_or_create(email=instance.email, defaults={
            'first_name': instance.first_name,
            'last_name': instance.last_name
        })


class BilletOption(models.Model):
    """
    Permet de relier une option à un billet
    """

    class Meta:
        verbose_name = _('lien entre une option et un billet')

    billet = models.ForeignKey('Billet', null=True, blank=True, related_name='billet_options', on_delete=models.CASCADE)
    option = models.ForeignKey(Option, on_delete=models.CASCADE)
    amount = models.IntegerField(default=1)
    participant = models.ForeignKey('Participant', null=True, blank=True, related_name='options_by_billet',
                                    on_delete=models.CASCADE)


class Billet(models.Model):
    product = models.ForeignKey(Product, null=True, blank=True, related_name='billets', on_delete=models.CASCADE)
    options = models.ManyToManyField(Option, through=BilletOption, related_name='billets')
    order = models.ForeignKey('Order', null=True, related_name='billets', on_delete=models.CASCADE)
    canceled = models.BooleanField(default=False)
    refunded = models.BooleanField(default=False)
    scanned = models.BooleanField(default=False)

    @staticmethod
    def validated():
        return Billet.objects.filter(
            order__status__lt=Order.STATUS_VALIDATED, order__created_at__gte=timezone.now() - timedelta(minutes=20)
        ) | Billet.objects.filter(order__status=Order.STATUS_VALIDATED, canceled=False)

    def __str__(self):
        return str("Billet n°" + str(self.id))


@receiver(pre_save, sender=Billet)
def before_save_billet_check_refund_status(sender, instance, raw, **kwargs):
    if instance.refunded and not instance.canceled:
        instance.canceled = True


class PricingRule(models.Model):
    class Meta:
        verbose_name = _('Règles de produits (Jauges/Limite)')

    TYPE_T = "MaxSeats"
    TYPE_BYTI = "MaxProductByOrder"
    TYPE_BYI = "CheckMaxProductForInvite"
    TYPE_VA = "VA"
    TYPE_VA_PERMANENCIER = "VApermanencier"
    RULES = (
        (TYPE_BYI, _("Vérifie la limite par rapport aux invitations")),
        (TYPE_BYTI, _("Limite le nombre dans une commande")),
        (TYPE_T, _("Limite le nombre de personnes")),
        (TYPE_VA, _("Validation VA")),
        (TYPE_VA_PERMANENCIER, _("Validation VA uniquement pour les permanences"))
    )
    """
        :var type: Le type de règle
        :var description: Sa description
        :var value: ...
    """
    type = models.CharField(max_length=50, choices=RULES)
    description = models.TextField()
    value = models.IntegerField()

    def __str__(self):
        if self.description:
            return self.description
        return str(self.type) + " " + str(self.value)

    def validate(self, order, permanencier=False):
        """
        Valide l'application de la règle sur une commande
        :type order: Order
        :return:
        """
        if self.type == PricingRule.TYPE_T:
            count = 0
            for pricing in self.pricings:
                count += pricing.reserved_seats()
            return count <= self.value
        elif self.type == PricingRule.TYPE_BYI:
            try:
                # noinspection PyTypeChecker
                invitation: Invitation = order.client.invitations.get(event=order.event)
                return invitation.seats - invitation.bought_seats >= 0
            except Invitation.DoesNotExist:
                return False
        elif self.type == PricingRule.TYPE_BYTI:
            count = 0
            for pricing in self.pricings:
                count += pricing.reserved_units(order.billets.all())
            return count <= self.value
        elif self.type == PricingRule.TYPE_VA_PERMANENCIER:
            if permanencier:
                if order.client.numero_VA:
                    va_product, va_options = 0, 0  # nbre d'utilisations
                    for commande in order.client.orders.filter(
                            event=order.event):  # parcourt toutes les commandes du client
                        for billet in commande.billets.all():  # parcourt les billets de la commande, puis les produits et options de chaque billet
                            # va_product=billet.product.filter(rules=self).count()
                            if self in billet.product.rules.all():
                                va_product += 1
                    if (va_product <= 1 and va_options <= 1) and va_options + va_product <= 1:
                        return True  # si le mec a utilisé moins de 2 réductions et qu'elles ne sont pas toutes les deux
                    # pour un produit ou pour une option
                    else:
                        return False

                else:
                    return False
            else:
                return False
            pass
        elif self.type == PricingRule.TYPE_VA:
            if order.client.numero_VA and not order.client.va_validating:
                va_product, va_options = 0, 0  # nbre d'utilisations
                for commande in order.client.orders.filter(
                        event=order.event):  # parcourt toutes les commandes du client
                    for billet in commande.billets.all():
                        for rule in billet.product.rules.all():
                            if rule.type == PricingRule.TYPE_VA or rule.type == PricingRule.TYPE_VA_PERMANENCIER:
                                va_product += 1
                    # en fait le code en dessous marche sur mon ordi mais pas sur Rancher ... (alors que j'ai Postgres sur les 2)
                    # va_product += commande.billets.filter(
                    #    product__rules__type__in=[self, PricingRule.TYPE_VA_PERMANENCIER]).count() # allez Postgres au travail !
                #
                # va_options += BilletOption.objects.filter(billet__order=commande, #et une 2e !
                #                                          option__type__in=[self, PricingRule.TYPE_VA_PERMANENCIER]).count()
                if (va_product <= 1 and va_options <= 1) and va_options + va_product <= 1:
                    return True  # si le mec a utilisé moins de 2 réductions et qu'elles ne sont pas toutes les deux
                # pour un produit ou pour une option
                else:
                    return False
            else:
                return False
        else:
            return False

    @property
    def pricings(self):
        return list(set(Product.objects.filter(rules=self)).union(set(Option.objects.filter(rules=self))))


class Coupon(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    code = models.CharField(max_length=20)
    description = models.CharField(max_length=255)
    max_use = models.IntegerField(default=0)
    percentage = models.FloatField(verbose_name=_('pourcentage'), help_text=_('entre 0 et 1. Attention, cumulable avec montant !'), default=0, blank=True)
    amount = models.FloatField(verbose_name=_('montant'), help_text=_('en euros. Attention, cumulable avec pourcentage !'), default=0, blank=True)
    products = models.ManyToManyField(Product, blank=True)
    options = models.ManyToManyField(Option, blank=True)

    def __str__(self):
        return '{} - {} (-{}€ et -{}%)'.format(self.code, self.description, self.amount, self.percentage * 100)

def avatar_storagename(instance,user_filename):
    return f"avatar_{instance.id}_{user_filename}"

class Participant(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.CharField(max_length=255, blank=True)
    phone = models.CharField(max_length=255, blank=True)
    billet = models.ForeignKey(Billet, related_name='participants', on_delete=models.CASCADE)
    avatar = models.ImageField(verbose_name='Photo de profil', null=True, blank=True, upload_to=avatar_storagename)

    def __str__(self):
        return self.first_name + " " + self.last_name + "  Billet N°" + str(self.billet.id)

    @property
    def nomprenom(self):
        return self.first_name + " " + self.last_name

    def order_status(self):
        status = dict(Order.STATUSES).get(self.billet.order.status)
        if status == "Confirmée":
            return "Payé"
        else:
            return "Non payé"

class Question(models.Model):
    QUESTIONS_TYPES = (
        (0, _('Champ libre')),
        (1, _('Champ libre (long)')),
        (2, _('Choix multiple')),
        (3, _('Choix multiple et libre')),
        (4, _('Choix')),
        (5, _('Date')),
        (6, _('Date et heure')),
        (7, _('Heure')),
        (8, _('Photo de profil'))
    )

    question = models.CharField(max_length=255)
    help_text = models.TextField(blank=True)
    data = models.TextField(blank=True, null=True)
    question_type = models.IntegerField(verbose_name=_('type de question'), choices=QUESTIONS_TYPES)
    required = models.BooleanField(default=False)
    target = models.CharField(max_length=30, choices=TARGETS, default='Participant')

    def __str__(self):
        return self.question


class Answer(models.Model):
    order = models.ForeignKey('Order', related_name='answers', on_delete=models.CASCADE)
    question = models.ForeignKey(Question, related_name='answers', on_delete=models.CASCADE)
    participant = models.ForeignKey(Participant, null=True, blank=True, on_delete=models.CASCADE)
    billet = models.ForeignKey(Billet, null=True, blank=True, on_delete=models.CASCADE)
    value = models.TextField(blank=True, null=True)


class Response(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE)
    data = models.TextField()


class PaymentMethod(models.Model):
    class Meta:
        verbose_name = _('Moyens de paiement')

    PROTOCOLS = (
        ("CB", _("payment by Credit Card")),
        ("ESP", _("payment by cash")),
        ("CHQ", _("payment by check")),
        ("VIR", _("payment by bank transfer"))
    )
    paymentProtocol = models.CharField(max_length=50, choices=PROTOCOLS)
    paymentMin = models.IntegerField(default=-1000000)
    paymentMax = models.IntegerField(default=1000000)

    def __str__(self):
        return self.paymentProtocol


class Client(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, unique=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    user = models.OneToOneField(User, related_name='client', blank=True, on_delete=models.CASCADE)
    numero_VA = models.CharField(verbose_name='Numéro de carte VA si applicable', null=True, blank=True, max_length=255)
    id_adhesion = models.IntegerField(verbose_name="ID sur Adhésion", null=True, blank=True, unique=True)
    va_validating = models.BooleanField(verbose_name="Carte VA en cours de validation", default=False)

    def __str__(self):
        name = "#" + str(self.id) + " "
        name += self.last_name + " "
        name += self.first_name + " ("
        name += self.email + ")"
        return name

    @property
    def name(self):
        name = self.last_name + " "
        name += self.first_name
        return name


@receiver(pre_save, sender=Client)
def before_save_client_map_user(instance, **kwargs):
    if instance.user_id is None:
        user = None
        try:
            user = User.objects.get(email=instance.email)
        except User.DoesNotExist:
            user = User.objects.create_user(instance.email, instance.email, get_random_string(length=32))
            user.save()
        instance.user = user


class Order(models.Model):
    class Meta:
        verbose_name = _("commande")

    STATUS_NOT_READY = 0
    STATUS_SELECT_PRODUCT = 1
    STATUS_SELECT_PARTICIPANT = 2
    STATUS_SELECT_QUESTION = 3
    STATUS_SELECT_OPTIONS = 4
    STATUS_REVIEW_ORDER = 5
    STATUS_PAYMENT = 6
    STATUS_VALIDATED = 7
    STATUS_REJECTED = 8
    STATUS_CANCELED = 9

    STATUSES = (
        (STATUS_NOT_READY, _('Pas initialisée')),
        (STATUS_SELECT_PRODUCT, _('Sélection des produits')),
        (STATUS_SELECT_OPTIONS, _('Sélection des options')),
        (STATUS_PAYMENT, _('Paiement')),
        (STATUS_VALIDATED, _('Confirmée')),
        (STATUS_REVIEW_ORDER, _('Commande en cours de confirmation par le client')),
        (STATUS_SELECT_PARTICIPANT, _('Sélection des participants')),
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    client = models.ForeignKey(Client, blank=True, null=True, related_name="orders", on_delete=models.CASCADE)
    coupon = models.ForeignKey(Coupon, blank=True, null=True, related_name="orders", on_delete=models.CASCADE)
    status = models.IntegerField(verbose_name=_('status'), default=0, choices=STATUSES)
    transaction = models.ForeignKey(TransactionRequest, default=None, null=True, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    @staticmethod
    def accountable_orders():
        """
        Définie les commandes devant être prises en compte dans le cadre de calcules de stock
        :return:
        """
        return (
                Order.objects.filter(status=Order.STATUS_VALIDATED) |
                Order.objects.filter(status=Order.STATUS_PAYMENT) |
                Order.objects.filter(
                    status__lt=Order.STATUS_VALIDATED,
                    created_at__gte=timezone.now() - timedelta(minutes=20))
        )

    def destroy_all(self):
        for billet in self.billets.all():
            for option in billet.billet_options.all():
                option.delete()
            billet.delete()
        self.delete()

    def can_use_coupon(self, coupon):
        return (coupon.max_use <= 0 or
                Order.accountable_orders().filter(
                    coupon=coupon,
                    created_at__lt=self.created_at
                ).count() < coupon.max_use)

    @property
    def option_billet(self):
        return self.billets.get_or_create(product=None)

    def is_valid(self, permanencier=False):
        rules = self.sold_products_rules
        if self.billets.filter(product__selling_mode='L').count() > 0:
            return False
        for rule in list(rules):
            if not rule.validate(self, permanencier):  # ça valide aussi la partie VA ....
                return False
        if self.billets.filter(product__selling_mode='I').count() > 0:
            if not permanencier:  # y'a pas d'invitationGrant pour une transaction permanencier
                bought_I = self.billets.filter(product__selling_mode='I', refunded=False, canceled=False).count()
                allowed_I = InvitationGrant.objects.filter(invitation__in=self.client.invitations.all()) \
                    .aggregate(total=Sum('amount'))['total']
                return bought_I <= allowed_I
        return True

    @property
    def amount(self):
        amount = 0.0
        products = set(self.products)
        options = set(self.options)
        if self.coupon_id:
            products_for_coupon = Coupon.objects.get(id=self.coupon_id).products.all()
            options_for_coupon = Coupon.objects.get(id=self.coupon_id).options.all()
        for product in list(products):
            temp_price = float(product.reserved_units(self.billets.all()) * product.price_ttc)
            if self.coupon_id and product in products_for_coupon:
                temp_price -= float(self.coupon.amount)
                temp_price *= float(1 - self.coupon.percentage)
            amount += temp_price
        for option in list(options):
            temp_price = float(option.reserved_units(self.billets.all()) * option.price_ttc)
            if self.coupon_id and option in options_for_coupon:
                temp_price -= float(self.coupon.amount)
                temp_price *= float(1 - self.coupon.percentage)
            amount += temp_price
        return amount

    @property
    def amount_ht(self):
        amount = 0.0
        products = set(self.products)
        options = set(self.options)
        if self.coupon_id:
            products_for_coupon = Coupon.objects.get(id=self.coupon_id).products.all()
            options_for_coupon = Coupon.objects.get(id=self.coupon_id).options.all()
        for product in list(products):
            temp_price = float(product.reserved_units(self.billets.all()) * product.price_ht)
            if self.coupon_id and product in products_for_coupon:
                temp_price -= float(self.coupon.amount)
                temp_price *= float(1 - self.coupon.percentage)
            amount += temp_price
        for option in list(options):
            temp_price = float(option.reserved_units(self.billets.all()) * option.price_ht)
            if self.coupon_id and option in options_for_coupon:
                temp_price -= float(self.coupon.amount)
                temp_price *= float(1 - self.coupon.percentage)
            amount += temp_price
        return amount

    @property
    def sold_products_rules(self):
        rules = set()
        for products in self.products:
            rules = rules.union(set(products.rules.all()))
        for option in self.options:
            rules = rules.union(set(option.rules.all()))
        return rules

    @property
    def sold_products(self):
        return set(self.products).union(set(self.options))

    @property
    def options(self):
        return Option.objects.filter(billetoption__billet__in=self.billets.all())

    @property
    def products(self):
        return Product.objects.filter(billets__in=self.billets.all())

    def __str__(self):
        return "Commande #" + str(self.event.id) + "-" + str(self.id)

    def send_tickets(self):
        email = TicketsEmail(self, to=(self.client.email,))
        email.send(True)

    def send_tickets_reminder(self):
        email = TicketsReminderEmail(self, to=(self.client.email,))
        email.send(True)

    @property
    def status_verbose(self):
        for s in self.STATUSES:
            if s[0] == self.status:
                return s[1]
        return "Pas initialisée"
        # return self.STATUSES[self.status][1]


@receiver(post_save, sender=TransactionMercanet)
def update_order_on_card_transaction(instance, **kwargs):
    try:
        order = Order.objects.get(transaction__mercanet=instance)
        request_status = instance.request.status
        if request_status == TransactionRequest.STATUSES['PAYED']:
            order.status = Order.STATUS_VALIDATED
            grants = InvitationGrant.objects.filter(invitation__client=order.client, product__in=order.products)
            for grant in grants:
                grant.amount -= order.billets.filter(product=grant.product).count()
                grant.save()
            order.send_tickets()
        elif request_status == TransactionRequest.STATUSES['REJECTED']:
            order.status = Order.STATUS_REJECTED
        order.save()
    except Order.DoesNotExist:
        pass


class Transaction(models.Model):
    class Meta:
        verbose_name = _("Transaction physiques au guichet (CB,espèces)")

    amount = models.DecimalField(verbose_name=_('montant en euros'), decimal_places=2, max_digits=12)
    moyen = models.ForeignKey(PaymentMethod, verbose_name="Moyen de paiement", on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True, verbose_name="Date/Heure de la transaction")
    # permanencier = models.ForeignKey(User, verbose_name="Personne qui a fait la transaction", on_delete=models.CASCADE)
    order = models.OneToOneField(Order, verbose_name="Commande associée", related_name="transaction_guichet", default=0,
                                 on_delete=models.CASCADE)
    permanence = models.ForeignKey(to='Permanence', verbose_name='Permanence lors de laquelle la vente a été effectuée',
                                   on_delete=models.CASCADE, related_name='transactions', null=True)

    def __str__(self):
        return ("Transaction n° {}   {}   {} €").format(self.id, self.moyen.paymentProtocol, self.amount)


class Table(models.Model):
    nom_table = models.CharField(verbose_name="Nom de la table", max_length=1023)

    class Meta:
        verbose_name_plural = 'Noms des tables'

    def __str__(self):
        return '#' + str(self.id) + ' ' + self.nom_table


class Place(models.Model):
    participant = models.OneToOneField(to=Participant, on_delete=models.CASCADE, related_name='place', verbose_name='Participant')
    place = models.IntegerField(verbose_name='Numéro de la place', default=0)
    table = models.ForeignKey(to=Table, on_delete=models.CASCADE, related_name='tables', verbose_name='Table')
    repas_particulier = models.BooleanField(default=False, verbose_name="Repas particulier")

    def __str__(self):
        return 'place' + str(self.place) + ' table ' + str(
            self.table.id) + ' "' + self.table.nom_table+'"'


class VA(models.Model):
    event = models.ForeignKey(Event, verbose_name="event", on_delete=models.CASCADE)
    VA = models.CharField(max_length=13)

    def __str__(self):
        return '#' + str(self.id) + ' VA: ' + str(self.VA) + ' event: ' + str(self.event.name)


class Stand(models.Model):
    class Meta:
        verbose_name = "Point de vente"

    name = models.CharField(max_length=2048, verbose_name='Nom du point de vente')
    lieu = models.CharField(max_length=2048, verbose_name="Lieu du point de vente", blank=True)

    def __str__(self):
        return "{} ({})".format(self.name, self.lieu)


class Permanence(models.Model):
    stand = models.ForeignKey(to=Stand, verbose_name="Point de vente", related_name='permanences',
                              on_delete=models.CASCADE)
    permanencier = models.ForeignKey(to=Membership, on_delete=models.CASCADE, verbose_name="Orga",
                                     related_name='permanences')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Heure de début")
    closed_at = models.DateTimeField(verbose_name="Heure de fin", blank=True, null=True)
    event = models.ForeignKey(to=Event, related_name="permanences", verbose_name="Évènement", on_delete=models.CASCADE)
    file = models.ForeignKey(to=File, related_name="permanences", verbose_name="File", default=1,
                             on_delete=models.CASCADE)

    def __str__(self):
        return "{} au stand {}".format(self.permanencier, self.stand)

    @staticmethod
    def filter_open(queryset):
        # return queryset.filter(created_at__lte=timezone.now())
        return queryset.filter(created_at__lte=timezone.now(), closed_at=None) | \
               queryset.filter(created_at__lte=timezone.now(), closed_at__gte=timezone.now())

    @staticmethod
    def from_user(user: User):
        return Permanence.filter_open(user.membership.permanences.all()).first()


def composter(billet: Billet, file: File) -> Compostage:
    assert Compostage.objects.filter(billet=billet, file=file).count() < 1, "Billet déjà composté dans cette file"
    assert file.active, "File non valide"
    assert file.event == billet.order.event, "Mauvais évènement"
    if file.product_type is not None:
        assert billet.product in file.product_type.all(), "Produit non valable dans cette file"
    if file.option_type is not None:
        assert billet.billet_options.filter(
            option__in=file.option_type.all()).count() == billet.billet_options.all().count(), "Option(s) non valable(s) dans cette file"  # vérifie que cette file peut valider toutes les options du billet
    # la série de pré-vérification est finie, place au vrai compostage
    if file.validation_type == 3:  # lecture de numéro de place
        return Compostage(billet=billet, file=file)
    if file.validation_type == 2: raise NotImplementedError("Il n'est pas encore possible de valider des options")
    if file.validation_type == 1:  # compostage de produits
        return Compostage.objects.create(billet=billet, file=file)
