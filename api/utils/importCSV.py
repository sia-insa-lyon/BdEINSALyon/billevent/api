import csv, sys
# Ce script va préparer le CSV pour qu'il ressemble à une base de données, et qu'on puisse l'importer directement dans Django
inputFile = 'GALA.csv'
outputFile = 'pourDjango.csv'
def parse_row(row):
    order = row[0]
    table = row[1]
    places = row[2]
    return order, table, places

def new_row(order, place, table):
    out = []
    out.append(order)
    out.append(place)
    out.append(table)
    return out

def create_tables_db(tables):
    sortie_tables = []
    i=0
    for table in set(tables):
        sortie_table = []
        sortie_table.append(i)
        sortie_table.append(table)
        i+=1
        sortie_tables.append(sortie_table)
        return sortie_tables

def search_for_id(sortie_tables, nom_table):
    for i in sortie_tables:
        if i[1] == nom_table
            return i[0]
ofile  = open(outputFile, "w")
write = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
#on charge le CSV dans la mémoire et on le transforme
sortie_places = []

with open(inputFile, 'r') as f:
    reader = csv.reader(f)
    tables = [] #liste de toutes les tables par apparition
    for row in reader: #première itération pour remplir la liste des tables
        order, table, places= parse_row(row)
        tables.append(table)
    sortie_tables = create_tables_db(tables) #assigne un ID à une table
    for row in reader:  #2e itération pour remplir les places avec un id de table
        order, table, places = parse_row(row)
        id_table = search_for_id(sortie_tables, table)
        for place in places.split(' '):
            sortie.append(new_row(order, place, id_table))
    f.close()

for row in sortie: # on écrit le CSV dans un ficher
    write.writerow(row)

ofile.close()