import six
from django.contrib.auth.tokens import PasswordResetTokenGenerator


class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, client, timestamp):
        return (
                six.text_type(client.pk) + six.text_type(timestamp)
        )


va_activation_token = TokenGenerator()
