from django.contrib.auth.models import User, Group
from django.core.signing import TimestampSigner
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from api import models
from api.models import Billet, Product, Transaction, Option, Compostage, File

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File;
        fields = "__all__"

class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = "__all__"
        #depth = 2

    #email = serializers.EmailField(write_only=True)
    #first_name = serializers.CharField(write_only=True)
    #last_name = serializers.CharField(write_only=True)
    #def create(self, validated_data):
    #    validated_data.pop('email')
    #    validated_data.pop('first_name')
    #    validated_data.pop('last_name')
    #    transaction = Transaction.objects.create(**validated_data)
    #    return transaction


class CompostageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Compostage
        fields = ("id","billet","file")
        depth = 0

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class PricingRuleSerializer(serializers.ModelSerializer):
    class Meta:
        read_only = True
        model = models.PricingRule
        fields = ('id', 'type', 'value')





class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        read_only = True
        model = models.Question
        fields = ('id', 'question', 'help_text', 'data', 'question_type', 'required', 'target')


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        read_only = True
        model = models.Answer
        fields = ('id', 'participant', 'question', 'value', 'order', 'billet')


class OptionSerializer(serializers.ModelSerializer):
    rules = PricingRuleSerializer(many=True, read_only=True)
    questions = QuestionSerializer(many=True, read_only=True)

    class Meta:
        model = models.Option
        fields = ('id', 'name', 'type', 'description',
                  'price_ht', 'price_ttc',
                  'rules', 'seats', 'target',
                  'event', 'how_many_left', 'questions')


class ParticipantSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False) #On en a besoin pour savoir si le particpant existe
    class Meta:
        model = models.Participant
        fields = ('id', 'first_name', 'last_name', 'phone', 'email', 'billet')


class ProductSerializer(serializers.ModelSerializer):
    rules = PricingRuleSerializer(many=True, read_only=True)
    options = OptionSerializer(many=True, read_only=True)
    questions = QuestionSerializer(many=True, read_only=True)

    class Meta:
        model = models.Product
        fields = ('id', 'name',
                  'price_ht', 'price_ttc', 'selling_mode',
                  'rules', 'options', 'seats', 'description',
                  'questions', 'event', 'how_many_left', 'categorie', 'type_liste_attente')


class CouponSerializer(serializers.ModelSerializer):
    class Meta:
        read_only = True
        model = models.Coupon
        fields = ('id', 'percentage', 'amount', 'description', 'products','options')


class OrganizerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Organizer
        fields = ('id', 'name', 'phone', 'address', 'email')


class EventSerializer(serializers.ModelSerializer):
    organizer = OrganizerSerializer()

    class Meta:
        model = models.Event
        fields = ('id', 'name','visibility', 'description',
                  'sales_opening', 'sales_closing',
                  'start_time', 'end_time',
                  'website', 'place', 'address', 'organizer',
                  'logo_url', 'ui_banner')
        depth = 10


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = ('id', 'first_name', 'last_name', 'email', 'phone', 'numero_VA', 'va_validating')


class InvitationGrantSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.InvitationGrant
        fields = ('product_id', 'amount')

    #def to_representation(self, instance:models.InvitationGrant):
    # pour ne pas montrer les invitationGrants déjà utilisés
    # problème: ce code ne marche que pour un grant par produit
    #    rep = super().to_representation(instance)
    #    rep['amount'] = instance.invitation.grants.filter(product=instance.product).count() \
    #                    - models.Billet.validated().filter(
    #        product=instance.product,
    #        order__in=instance.invitation.client.orders.filter(event=instance.invitation.event)).count()
    #    return rep


class InvitationSerializer(serializers.ModelSerializer):
    event = EventSerializer(read_only=True)
    client = ClientSerializer(read_only=True)
    grants = InvitationGrantSerializer(read_only=True, many=True)

    class Meta:
        model = models.Invitation
        fields = ('id', 'client', 'event', 'token', 'bought_seats', 'seats', 'grants')
        depth = 3


class BilletOptionSerializer(serializers.ModelSerializer):
    option = OptionSerializer()

    class Meta:
        model = models.BilletOption
        fields = ('id', 'option', 'participant', 'amount', 'billet')


class BilletOptionInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BilletOption
        fields = ('id', 'option', 'participant', 'amount', 'billet')


class BilletSerializer(serializers.ModelSerializer):
    product = PrimaryKeyRelatedField(many=False, queryset=Product.objects.all())
    options = PrimaryKeyRelatedField(many=True, read_only=True, required=False)
    participants = ParticipantSerializer(many=True, required=False)
    billet_options = BilletOptionSerializer(many=True, required=False)

    class Meta:
        model = models.Billet
        fields = ('id', 'product', 'options', 'billet_options', 'participants')
        depth = 2


class BilletForOrderSerializer(serializers.ModelSerializer):
    product = ProductSerializer(many=False)
    billet_options = BilletOptionSerializer(many=True, required=False)
    options = PrimaryKeyRelatedField(many=True, read_only=True, required=False)
    participants = ParticipantSerializer(many=True, required=False)

    class Meta:
        model = models.Billet
        fields = ('id', 'product', 'options', 'billet_options', 'participants')
        depth = 2


class OrderSerializer(serializers.ModelSerializer):
    event = EventSerializer(read_only=True)
    billets = BilletForOrderSerializer(many=True, read_only=True)
    answers = AnswerSerializer(many=True, read_only=True)
    client = ClientSerializer(read_only=True)
    coupon = CouponSerializer(read_only=True)
    signed_id = serializers.SerializerMethodField()

    def get_signed_id(self, order):
        return TimestampSigner().sign(order.id)

    class Meta:
        model = models.Order
        fields = ('id', 'client', 'event', 'billets', 'status', 'answers', 'coupon','signed_id')
        depth = 2


class CategorieSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)

    class Meta:
        model = models.Categorie
        fields = ('name', 'desc', 'products', 'hide_on_full')


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Place
        fields = ('place', 'table', 'participant', 'repas_particulier')
        depth = 1 #pour renvoyer le nom de la table et pas juste son id

class PlaceInsertSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Place
        fields = ('particpant', 'place', 'table')

from api.models import Event, Client
class InvitationRAIDSerializer(serializers.ModelSerializer):
    client = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all())
    event =  serializers.PrimaryKeyRelatedField(queryset=Event.objects.all())
    #client = serializers.PrimaryKeyRelatedField(many=True)
    #event =  serializers.PrimaryKeyRelatedField(many=True)
    class Meta:
        model = models.Invitation
        fields = ('id', 'client', 'first_name', 'last_name', 'email', 'reason', 'grants', 'event', 'token', 'seats', 'bought_seats', 'link_sent')
        depth = 1
    #grants = PrimaryKeyRelatedField

class InvitationGrantRAIDSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.InvitationGrant
        fields = '__all__'


class FullUserView(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password')

    def create(self, validated_data):
        user = super(FullUserView, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.username = user.email
        user.save()
        return user


class VASerializer(serializers.ModelSerializer):
    class Meta:
        model = models.VA
        fields = ('product', 'VA', 'amount')