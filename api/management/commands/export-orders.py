from api.models import *
from django.core.management.base import BaseCommand, CommandError, CommandParser
import csv, _csv

# print("N° Commande,Nom  (commande),Prénom  (commande),N° Billet,Produit,N° Participant,Nom (Participant),Prénom (Participant)")
# print(f"{o.id}, {o.client.last_name}, {o.client.first_name}, {b.id}, {b.product}, {p.id}, {p.last_name}, {p.first_name}")


class Command(BaseCommand):
    help = 'Exporte en CSV les données des commandes nécessaires au placement des repas du Gala par les orgas'

    def add_arguments(self, parser: CommandParser):
        parser.add_argument('--file', dest='file', type=str, nargs='?', help="fichier de sortie CSV",
                            default=None)  # type=argparse.FileType('w')
        parser.add_argument('--event-id', dest='event_id', type=int, help="ID de l'évènement")
        parser.add_argument("--product-ids", dest='product_ids', nargs='+', type=int,
                            help="IDs des produits dont il faut afficher les billets")

    def handle(self, *args, **options):
        out = open(options.get('file', 'export_placement.csv'), 'w')
        csv_writer: _csv.writer = csv.writer(out, dialect='excel')

        event_id = options.get('event_id', 7)
        products = Product.objects.filter(id__in=options.get('product_ids'), event=event_id)  # products = Product.objects.filter(name__in=procut_names_default).filter(event=event_id)
        options = Option.objects.filter(id__in=[13,14])
        orders = Order.objects.filter(status=Order.STATUS_VALIDATED).order_by('client')
        print(f"{orders.count()} commandes valides pour les produits:\n {', '.join([p['name'] for p in products.values('name')])}")
        csv_writer.writerow(
            ["N° Commande", "Nom  (commande)", "Prénom  (commande)", "N° Billet","Produit", "N° Participant",
             "Nom (Participant)", "Prénom (Participant)"]+[option.name for option in options])
        i=0
        pn=0
        for order in orders:
            o: Order = order
            i+=o.billets.filter(product__in=products).count()
            for billet in o.billets.filter(product__in=products, canceled=False, refunded=False):
                b: Billet = billet
                for participant in b.participants.all():
                    pn+=b.participants.count()
                    p: Participant = participant
                    csv_writer.writerow(
                        [o.id, o.client.last_name, o.client.first_name, b.id, b.product, p.id, p.last_name,
                         p.first_name]+[BilletOption.objects.filter(billet__order=o, billet__refunded=False, billet__canceled=False, option=option).aggregate(somme=Sum('amount'))['somme'] for option in options])
        out.close()
        print(f"{i} billets, {pn} participants")
