import _csv
import argparse
import csv
import sys

from django.core.management.base import BaseCommand, CommandParser

from api.models import *


class Command(BaseCommand):
    help = 'Exporte en CSV les données des commandes nécessaires au placement des repas du Gala par les orgas'
    """
    Dans l'excel de placement, il prend une ligne (n°table, n°place, n°commande) et remplace la commande par un de ses billets repas
    """

    def add_arguments(self, parser: CommandParser):
        parser.add_argument('--in', dest='infile', type=argparse.FileType('r'), nargs='?', help="fichier d'entrée CSV",
                            default=sys.stdin)  # type=argparse.FileType('w')
        parser.add_argument('--out', dest='outfile', nargs='?', type=argparse.FileType('w'),
                            help="fichier de sortie CSV", default=sys.stdout)

    def handle(self, *args, **options):
        inf = options.get('infile', 'export_placement.csv')
        out = options.get('outfile')
        csv_reader: _csv.reader = csv.reader(inf)
        csv_writer: _csv.writer = csv.writer(out, dialect='excel')
        next(csv_reader)  # saute l'en-tête
        all_orders = Order.objects.filter(status=Order.STATUS_VALIDATED)

        used_participants = []

        print(all_orders.count())
        for (nom_table, table_id, order_id, place_num) in csv_reader:
            order: Order = all_orders.get(id=order_id)
            order_billets_repas = order.billets.filter(product=43, refunded=False, canceled=False) #Participant.objects.filter(billet__order=order_id).exclude(id__in=used_participants)
            if order_billets_repas.count() > 0:
                participant:Participant = Participant.objects.filter(billet__in=order_billets_repas).exclude(id__in=used_participants).first()
                if participant is not None:
                    csv_writer.writerow([nom_table, table_id, participant.id, place_num, participant.first_name,participant.last_name])
                    used_participants.append(participant.id)
                else:
                    print(f"erreur a la ligne {nom_table},{place_num} pour {order_id}: pas de participant trouvé", file=sys.stderr)