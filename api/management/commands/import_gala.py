import csv

from django.core.management.base import BaseCommand, CommandParser

from api.models import *
from api.models import Invitation
from prdd.models import Student


class Command(BaseCommand):
    help = 'Exporte en CSV les données des commandes nécessaires au placement des repas du Gala par les orgas'

    def add_arguments(self, parser: CommandParser):
        parser.add_argument('--file', dest='file', type=str, nargs='?', help="fichier de sortie CSV",
                            default=None)  # type=argparse.FileType('w')
        parser.add_argument('--event-id', dest='event_id', type=int, help="ID de l'évènement")
        parser.add_argument('--seats', dest='seats', type=int, help="Nombre de places par invitations")

    def handle(self, *args, **options):
        filename = options.get('file', 'export_placement.csv')
        print(f"using {filename}")
        gala = Event.objects.get(visibility='invite', name__contains='GALA')
        event_id = options.get('event_id', None)
        if event_id is not None:
            gala = Event.objects.get(id=event_id)
        seats = options.get('seats', 1)
        with open(filename, 'r') as file:
            reader = csv.reader(file, delimiter=',', quotechar='"')
            for row in reader:
                prenom, nom, email, depart_id = row
                print(row)
                invit = Invitation(first_name=prenom,
                                   last_name=nom,
                                   email=email, seats=seats, reason='import', event=gala)
                invit.save()
                print(invit)
                s = Student(
                    first_name=prenom,
                    last_name=nom,
                    department_id=depart_id,
                    token=invit.token
                )
                s.save()
                print(s)
