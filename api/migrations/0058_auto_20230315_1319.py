# Generated by Django 3.1.1 on 2023-03-15 12:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0057_client_va_validating'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='va_validating',
            field=models.BooleanField(default=False, verbose_name='Carte VA en cours de validation'),
        ),
    ]
