# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-03-14 02:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0033_auto_20180314_0107'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='va',
            name='amount',
        ),
        migrations.RemoveField(
            model_name='va',
            name='product',
        ),
        migrations.AddField(
            model_name='va',
            name='event',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='api.Event', verbose_name='event'),
            preserve_default=False,
        ),
    ]
