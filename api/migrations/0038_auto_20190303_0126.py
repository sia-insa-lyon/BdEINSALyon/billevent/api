# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2019-03-03 00:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0037_auto_20190225_1248'),
    ]

    operations = [
        migrations.CreateModel(
            name='Permanence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Heure de début')),
                ('closed_at', models.DateTimeField(blank=True, null=True, verbose_name='Heure de fin')),
                ('permanencier', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='permanences', to='api.Membership', verbose_name='Orga')),
            ],
        ),
        migrations.CreateModel(
            name='Stand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=2048, verbose_name='Nom du point de vente')),
                ('lieu', models.CharField(blank=True, max_length=2048, verbose_name='Lieu du point de vente')),
            ],
            options={
                'verbose_name': 'Point de vente',
            },
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='permanencier',
        ),
        migrations.AddField(
            model_name='permanence',
            name='stand',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='permanences', to='api.Stand', verbose_name='Point de vente'),
        ),
        migrations.AddField(
            model_name='transaction',
            name='permanence',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='transactions', to='api.Permanence', verbose_name='Permanence lors de laquelle la vente a été effectuée'),
        ),
    ]
