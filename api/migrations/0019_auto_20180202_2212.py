# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-02-02 21:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0018_merge_20180202_2212'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='billet',
            name='files',
        ),
        migrations.AlterField(
            model_name='compostage',
            name='date',
            field=models.DateTimeField(auto_created=True, auto_now_add=True),
        ),
    ]
