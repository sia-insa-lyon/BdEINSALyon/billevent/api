# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2019-03-03 12:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0039_permanence_event'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='amount',
            field=models.DecimalField(decimal_places=2, max_digits=12, verbose_name='montant en euros'),
        ),
    ]
