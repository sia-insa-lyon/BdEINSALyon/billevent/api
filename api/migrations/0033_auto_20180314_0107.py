# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-03-14 00:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0032_auto_20180313_2325'),
    ]

    operations = [
        migrations.CreateModel(
            name='VA',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('VA', models.CharField(max_length=13)),
                ('amount', models.IntegerField(verbose_name='Nombre de produits VA autorisés')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.Product', verbose_name='Produit')),
            ],
        ),
        migrations.AlterField(
            model_name='pricingrule',
            name='type',
            field=models.CharField(choices=[('CheckMaxProductForInvite', 'Vérifie la limite par rapport aux invitations'), ('MaxProductByOrder', 'Limite le nombre dans une commande'), ('MaxSeats', 'Limite le nombre de personnes'), ('VA', 'WIP :Require VA validation')], max_length=50),
        ),
    ]
