from datetime import timedelta
from os.path import exists
from typing import Any

from django import urls
from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.signing import TimestampSigner, Signer, BadSignature
from django.db import transaction
from django.db.models import Q
from django.http import Http404
from django.utils import timezone
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.decorators.csrf import csrf_exempt
from django.http.response import FileResponse
from django.http import HttpResponseForbidden
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.decorators import authentication_classes, permission_classes, api_view
from rest_framework.exceptions import APIException, NotFound, UnsupportedMediaType
from rest_framework.generics import ListAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings

from api import permissions
from api.adhesion_api import AdhesionAPI
from api.email import VaValidationEmail
from api.models import Client
from api.models import Coupon, \
    Transaction, Place, VA
from api.models import Event, Order, Option, Product, Billet, Categorie, Invitation, File, BilletOption, Compostage, \
    Participant
from api.permissions import IsEventManager
from api.serializers.admin import ParticipantSearchSerializer
from api.serializers.public import BilletSerializer, CategorieSerializer, InvitationSerializer, ParticipantSerializer, \
    AnswerSerializer, BilletOptionInputSerializer, UserSerializer, TransactionSerializer, \
    CompostageSerializer, FileSerializer, PlaceSerializer
from api.serializers.public import EventSerializer, OrderSerializer, OptionSerializer, \
    ProductSerializer, FullUserView
from api.cron import clean_old_orders
from api.utils.tokens import va_activation_token
from billetterie.settings import BILLEVENT
from mercanet.models import TransactionRequest

plus_disponible_view = Response("Ce que vous demandez n'est plus disponible", status=status.HTTP_200_OK)
invalid_request_view = Response("Requête invalide, les paramètres spécifiés dans le POST sont non conformes",
                                status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(['GET', 'POST'])
@permission_classes((IsEventManager,))
@action(methods=["POST"], detail=True)
def billet_check(request):
    """
    Prend en paramètre POST id l'id signé du billet. Nécessite d'être authentifié en tant que manager de l'évenement

    :return: Le billet si il existe.
    """
    data = request.data['id']
    try:
        id = Signer().unsign(data)
    except BadSignature:
        return Response("Erreur de lecture", status=406)

    if Billet.objects.get(id=id).order.status == Order.STATUS_VALIDATED and not Billet.objects.get(
            id=id).canceled and not Billet.objects.get(id=id).refunded:
        return Response(BilletSerializer(Billet.objects.get(id=id)).data)
    return Response("Le billet n'est pas valide", status=401)


class CompostageViewSet(viewsets.ModelViewSet):
    """
    Le viewset pour les compostages: @see le model Compostage
       """
    queryset = Compostage.objects.all()
    serializer_class = CompostageSerializer
    permission_classes = [IsEventManager]

    def create(self, request, *args, **kwargs):
        try:
            file = File.objects.get(id=request.data['file'])

            if Compostage.objects.filter(billet=request.data["billet"], file=file.id).count() > 0:
                return Response("Attention ! Ce billet a déja été validé", status=409)

            if \
                    Billet.objects.get(id=request.data['billet']).product not in file.product_type.all() \
                            and not Billet.objects.get(id=request.data['billet']).options.filter(
                        id__in=file.option_type.values("id")).exists():
                return Response("Produit/Option non validé dans cette file ou à cet horaire", status=423)

            compostage = Compostage(billet=Billet.objects.get(id=request.data["billet"]), file=file)
            compostage.save()

            return Response(CompostageSerializer(compostage).data)

        except File.DoesNotExist as e:
            return Response("La file demandée n'existe pas !")

        except Billet.DoesNotExist as e:
            return Response("le billet demandé n'existe pas !")


class FileViewSet(viewsets.ModelViewSet):
    queryset = File.objects.all()
    serializer_class = FileSerializer

    def get_queryset(self):
        return File.objects.filter(event__in=self.request.user.membership.events.all())


class EventsViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Event.for_user(self.request.user)

    @action(methods=['get'], detail=True)
    def categorie(self, request, pk=None):
        """Permet de récupérer toutes les catégories liées à l'évenement
        """
        event = self.get_object()
        # On récupère toutes les catégories liées à l'évenement triées par ordre d'id
        return Response(CategorieSerializer(Categorie.objects.filter(event=event).order_by('pk'), many=True).data)

    @action(methods=['get'], detail=True)
    def invitation(self, request, pk):
        return Response(InvitationSerializer(request.user.client.invitations.get(event_id=pk)).data)

    @action(methods=['get', 'post'], detail=True)
    def order(self, request, pk=None):
        """Permet de créer une commande.

         Il faut envoyer un dictionaire dont la valeur "billets" correspond à un tableau contenat les billets
        """

        # On récupère l'évenement en s'assurant au passage que l'utilisateur a le droit  d'y accéder
        event = Event.for_user(request.user).get(id=pk)
        # On récupère le client lié à l'user
        client = request.user.client

        # On récupère la commande en cours si il en existe une, sinon on la crée
        order = client.orders.filter(event=event, status__lt=Order.STATUS_PAYMENT,
                                     created_at__gt=timezone.now() - timedelta(minutes=20)).first() or \
                Order(event=event, client=client, status=Order.STATUS_SELECT_PRODUCT)
        order.save()

        if request.method == 'GET':
            return Response(OrderSerializer(order).data)

        # On place le status en sélection de produit
        order.status = order.STATUS_SELECT_PRODUCT
        order.save()

        order.billets.all().delete()

        # Si il y a un champ billets dans les données renvoyées
        if "billets" in request.data:
            # Pour chaque billet dans l'array de billets
            for billet in request.data['billets']:
                # Si le billet est update
                if "id" not in billet:
                    billet_data = BilletSerializer(data=billet, context={"order": order.id})
                    if billet_data.is_valid():
                        billet_data.validated_data['order'] = order
                        billet = billet_data.create(billet_data.validated_data)
                        billet.save()

        ok = order.is_valid()
        # Si la commande ne répond pas aux règles
        if not ok:
            order.destroy_all()
            return Response("NIQUE TA MERE ESSAYE PAS DE GRUGER", status=400)

        order.status = order.STATUS_SELECT_PARTICIPANT
        order.save()

        return Response(OrderSerializer(order).data)

    @action(methods=['post'], detail=True)
    def options(self, request: Request, pk=None, *args, **kwargs):

        event: Event = Event.for_user(request.user).get(id=pk)
        client: Client = request.user.client
        order: Order = client.orders.get(event=event, status__lt=Order.STATUS_PAYMENT)
        transaction.atomic()
        for optionbillet in request.data:
            if "billet" in optionbillet and optionbillet['billet'].isDigit():
                # Récupère le billet et renvoie une erreur si le billet n'est pas dans la commande
                billet = order.billets.get(id=int(optionbillet['billet']))
            else:
                billet = order.option_billet

            # Récupère l'option et renvoie une erreur si l' option n'est pas liée au produit du billet
            option = billet.product.options.get(id=int(optionbillet['option']))
            participant = None

            if "participant" in optionbillet and optionbillet['participant'].isdigit():
                participant = billet.participants.get(id=int(optionbillet['participant']))

            BilletOption(billet=billet, option=option, amount=int(optionbillet['amount']),
                         participant=participant).save()

        if not order.is_valid():
            transaction.rollback()
            return Response("NIQUE TA MERE ESSAYE PAS DE GRUGER")

        order.status = order.STATUS_PAYMENT
        order.save()

        return Response(OrderSerializer(order).data)


"""
class OptionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Option.objects.all()
    serializer_class = OptionSerializer
"""


class TransactionViewSet(viewsets.ModelViewSet):
    serializer_class = TransactionSerializer
    permission_classes = [permissions.IsEventManager]

    def get_queryset(self):
        return Transaction.objects.filter()

    def create(self, request, *args, **kwargs):
        from api.email import TicketsEmail
        """
            Pour créer la transaction, il faut envoyer en plus first_name, last_name et email
        """
        # request.data['permanencier'] = request.user.id
        serializer = TransactionSerializer(data=request.data)

        if serializer.is_valid():
            transaction = serializer.save()
            order = Order.objects.get(id=transaction.order.id)
            if (not transaction.order.is_valid(permanencier=True)):
                return Response("Commande invalide !", status=403)
            if not (transaction.amount == order.amount * 100):
                return Response("Les sommes ne correspondent pas", status=403)

            order.status = Order.STATUS_VALIDATED
            transaction.save()
            # test = order.billets.all()
            billet = order.billets.all().last()
            # test2 = billet.participants.all()
            participant = billet.participants.all().last()  # on définit un mec comme Client
            order.client = Client.objects.create(first_name=participant.first_name, last_name=participant.last_name,
                                                 email=participant.email)
            email = TicketsEmail(to=(participant.email,), order=order)
            email.send()
            order.save()

            return Response(TransactionSerializer(transaction).data)
        # return Response("Une erreur est survenue ! Les données envoyées sont invalides", status=403)
        return Response(serializer.errors)


class OrderViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Order.objects.filter(client__user=self.request.user)

    @action(methods=['post'], detail=True)
    def coupon(self, request, **kwargs):
        order = self.get_object()
        req_code = request.data['code']
        if not req_code or req_code == '':
            order.coupon = None
            order.save()
            return Response(OrderSerializer(order).data)
        code = Coupon.objects.get(code=request.data['code'], event=order.event)
        if order.can_use_coupon(code):
            order.coupon = code
            order.save()
            return Response(OrderSerializer(order).data)
        return Response(status=404)

    @action(methods=['post'], detail=True)
    def participants(self, request, pk):
        order = self.get_object()

        # Pour chaque participant dans le JSON
        for participant_data in request.data:
            # On crée le sérializer et on regarde si il est valide, comme d'hab
            participant_ser = ParticipantSerializer(data=participant_data)
            if participant_ser.is_valid():
                participant = None
                if 'id' in participant_ser.validated_data: # Si l'id est dans les données validées
                    participant = Participant.objects.filter(id=participant_ser.validated_data['id']).first()
                    if participant is not None: # Si le participant existe
                        participant_ser.update(participant, participant_ser.validated_data) # On met à jour le participant
                    else: # Sinon on crée un nouveau participant
                        participant = participant_ser.create(participant_ser.validated_data)
                else: # Sinon on crée un nouveau participant
                    participant = participant_ser.create(participant_ser.validated_data)
                if participant.billet.product.seats >= participant.billet.participants.count():
                    participant.save()
                else:
                    participant.delete()
            else:
                return Response(participant_ser.errors)

        order.state = Order.STATUS_SELECT_QUESTION
        order.save()

        return Response(OrderSerializer(self.get_object()).data)

    @action(methods=['post'], detail=True)
    def answers(self, request, pk):
        order = self.get_object()

        order.answers.all().delete()

        # Pour chaque participant dans le JSON
        for answer_data in request.data:
            # On crée le sérializer et on regarde si il est valide, comme d'hab
            answer_data['order'] = pk
            answer_serializer = AnswerSerializer(data=answer_data)
            if answer_serializer.is_valid():
                answer = answer_serializer.create(answer_serializer.validated_data)
                answer.save()
            else:
                return Response(answer_serializer.errors)

        order.status = Order.STATUS_SELECT_OPTIONS
        order.save()

        return Response(OrderSerializer(order).data)

    @action(methods=['post'], detail=True)
    def optionsanswers(self, request, pk):
        order = self.get_object()
        for answer_data in request.data:
            answer_data['order'] = pk
            answer_serializer = AnswerSerializer(data=answer_data)
            if answer_serializer.is_valid():
                answer = answer_serializer.create(answer_serializer.validated_data)
                answer.save()
            else:
                return Response(answer_serializer.errors)
        order.save()
        return Response(OrderSerializer(order).data)

    @action(methods=['post'], detail=True)
    def billet_options(self, request, pk):
        order = self.get_object()

        for billet in order.billets.all():
            billet.billet_options.all().delete()

        # Pour chaque participant dans le JSON
        for bo_data in request.data:
            # On crée le sérializer et on regarde si il est valide, comme d'hab
            bo_data['order'] = pk
            if 'billet' not in bo_data or bo_data['billet'] is None:
                billet, _ = order.billets.get_or_create(product=None)
                bo_data['billet'] = billet.id
            bo_serializer = BilletOptionInputSerializer(data=bo_data)
            if bo_serializer.is_valid():
                bo = bo_serializer.create(bo_serializer.validated_data)
                bo.save()
            else:
                return Response(bo_serializer.errors)

        if order.is_valid():
            order.status = Order.STATUS_REVIEW_ORDER
            order.save()
            return Response(OrderSerializer(order).data)
        else:
            for billet in order.billets.all():
                billet.billet_options.all().delete()

            return Response({"error": "Les options dépassent les quotas"}, status=400)

    @action(methods=['post'], detail=True)
    def cancel(self, request, pk):
        order = self.get_object()
        order.status = Order.STATUS_CANCELED
        order.save()
        if order.delete():
            return Response({'success': True})
        else:
            return Response({'success': False})

    @action(methods=['post'], detail=True)
    def go_back(self, request, pk):
        order = self.get_object()
        if Order.STATUS_SELECT_PARTICIPANT <= order.status <= Order.STATUS_REVIEW_ORDER:
            if order.status == Order.STATUS_SELECT_OPTIONS: order.status -= 1 #On revient un cran de plus pour modif le participant
            order.status -= 1
            if order.status == Order.STATUS_SELECT_PRODUCT: order.billets.all().delete()
            order.save()
            return Response(OrderSerializer(order).data)
        Response({"error": "Impossible de revenir en arrière"}, status=400)

    @action(methods=['get'], detail=True)
    def final(self, request, pk):
        id = pk
        order = Order.objects.get(id=id, client__user=request.user)

        status = "waiting"
        if order.status == Order.STATUS_VALIDATED:
            for billet in order.billets.all():
                if billet.product is not None:
                    for rule in billet.product.rules.all():
                        if rule.type == "VA":
                            VA(event=order.event, VA=order.client.numero_VA).save()
            status = "validated"
        elif order.status >= Order.STATUS_REJECTED:
            status = "rejected"

        # tickets = request.build_absolute_uri(urls.reverse('ticket-print', args=[TimestampSigner().sign(id)]))
        tickets = BILLEVENT.get('API_URL') + urls.reverse('ticket-print', args=(TimestampSigner().sign(id),))

        return Response({
            'status': status,
            'url': tickets
        })

    @action(methods=['post'], detail=True)
    def pay(self, request, pk=None):
        """Renvoie l'url vers la page de paiement, prend en paramètre post "callback" une adresse (JSP LAQUELLE C)"""
        # On récupère la commande
        order = self.get_queryset().get(id=pk)
        # On récupère l'url de callback pour mercanet (Je sais pas ce que c'est mais c'est le front qui me l'envoie)
        callback = request.data['callback']

        amount = order.amount

        if amount > 0:
            # On crée une requête de transaction
            transaction_request = TransactionRequest(callback=callback, amount=amount * 100)
            transaction_request.save()

            # On change le statut de la commande à payé
            order.status = order.STATUS_PAYMENT
            order.transaction = transaction_request
            order.save()

            # On renvoie l'url de paiement
            return Response(request.build_absolute_uri(
                urls.reverse('mercanet-pay', args=[transaction_request.id, transaction_request.token])))
        else:
            order.status = order.STATUS_VALIDATED
            order.save()
            order.send_tickets()
            return Response(status=201)


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Le viewset pour les produits:

    Faire le requête avec le paramètre GET \'event\' permet de filtrer la requête par event.
    """

    serializer_class = ProductSerializer

    # permission_classes = permissions.IsAuthenticatedAndReadOnly

    def get_queryset(self):
        events = Event.for_user(self.request.user)

        event_id = self.request.GET.get("event")
        if event_id is None:
            return Product.objects.filter(event=events)

        return Product.objects.filter(event=events.filter(id=event_id))


class OptionViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Le viewset pour les option:

    Faire le requête avec le paramètre GET \'produit\' permet de filtrer la requête par produit.
    """

    serializer_class = OptionSerializer
    permission_classes = (permissions.IsAuthenticatedAndReadOnly,)

    def get_queryset(self):
        events = Event.for_user(self.request.user)

        event_id = self.request.GET.get("event")
        if event_id is None:
            return Option.objects.filter(event=events)

        return Option.objects.filter(event=events.get(event_id))


class BilletViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = BilletSerializer
    queryset = Billet.objects.all()

    def retrieve(self, request, pk=None, *args, **kwargs):
        return Response(BilletSerializer(self.queryset.filter(id=pk).get()).data)

    def create(self, request, *args, **kwargs):
        """
        Pour créer un nouveau billet, il faut envoyer une requête POST avec l'id du produit dans le champ product et la liste des ids d'options dans le champ options

        """
        try:
            if Product.objects.get(id=request.data['product']).how_many_left > 0 or Product.objects.get(
                    id=request.data['product']).how_many_left == -1:
                # On crée un sérializer contenant les data envoyés par l'utilisateur pour checker si ce qui est envoyé est bien un billet
                billet = BilletSerializer(data=request.data)
                if billet.is_valid():  # Si le billet est valide

                    billet.validated_data['id'] = Billet.objects.count() + 1

                    new_billet = billet.create(billet.validated_data)
                    # new_billet = Billet(id=Billet.objects.count()+1, product=Product.objects.get(id=billet.data['product']),
                    #                    options=Option.objects.filter(id=billet.data['options'][0]))

                    new_billet.save()

                    return Response(BilletSerializer(new_billet).data, status=status.HTTP_201_CREATED)
                return Response(billet.errors, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError as e:
            return Response(e.message)
        return Response("Plus de billets disponibles !", status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        """
        Pour mettre à jour un billet, voir aussi la fonction create. Attention, met seulement à jour les options !

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        billet = BilletSerializer(data=request.data)
        if billet.is_valid():
            try:
                ancien_billet = Billet.objects.get(id=kwargs.get("pk"))
                test = billet.validated_data
                # billet.validated_data['product']=ancien_billet.product.id
                # ancien_billet.options = billet.data['options']
                test = billet.update(ancien_billet, billet.validated_data)
                ancien_billet.save()
                return Response(BilletSerializer(ancien_billet).data, status=status.HTTP_201_CREATED)
            except ValidationError as e:
                return Response(e.message)
        return invalid_request_view


@permission_classes([])
class RulesViews(APIView):
    def post(self, request):
        data = request.data
        compute = data['compute']
        data = data['data']

        if compute == 'MaxSeats':
            pricings = list(set(Product.objects.filter(id__in=data['products']))
                            .union(set(Option.objects.filter(id__in=data['options']))))
            count = 0
            for pricing in pricings:
                count += pricing.reserved_seats()
            return Response({'value': count})
        elif compute == 'InvitationsUsed':
            invitation = Invitation.objects.get(event=data['event'], client=request.user.client)
            return Response({'value': invitation.bought_seats, 'limit': invitation.seats})
        elif compute == 'VaUsed':
            if VA.objects.filter(VA=request.user.client.numero_VA, event=data['event']) > 0:
                return Response({'value': "true"})
            else:
                return Response({'value': "false"})

        else:
            return Response(data)


@permission_classes([])
class CurrentUserViews(APIView):
    def get(self, request):
        if not request.user:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        return Response(UserSerializer(request.user).data)


@permission_classes([])
class LogoutViews(APIView):
    def delete(self, request):
        if not request.user:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        logout(request.user)
        return Response(UserSerializer(request.user).data)


jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER

jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


# noinspection PyMethodMayBeStatic
@authentication_classes([])
@permission_classes([])
class InvitationAuthentication(APIView):
    """
    Authenticate a user based on an invitation token
    """

    def post(self, request):

        if 'token' not in request.data:
            a = APIException(detail='No token given')
            a.status_code = 400
            raise a
        token = request.data['token']
        try:
            invitation = Invitation.objects.get(token=token)
        except Invitation.DoesNotExist:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)

        invitation_serializer = InvitationSerializer(invitation)

        payload = jwt_payload_handler(invitation.client.user)
        jwt_token = jwt_encode_handler(payload)

        return Response({
            'jwt': jwt_token,
            'invitation': invitation_serializer.data
        }, status=status.HTTP_202_ACCEPTED)


class PlaceView(ListAPIView):
    serializer_class = PlaceSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        try:
            billet_id = self.kwargs['id']  # on prend l'ID depuis l'URL
            order_id = Billet.objects.get(pk=billet_id).order
            # places = Place.objects.filter(participant__in=Billet.objects.get(pk=billet_id).participants.all())
            places = Place.objects.filter(participant__in=Participant.objects.filter(billet__order=order_id))
            if places.count() > 0:  # on vérifie qu'il y a des places liées à la commande
                return places  # on donne le queryset à ListAPIView
            else:
                raise UnsupportedMediaType('text', detail="Pas de places associées à ce billet")
        except Billet.DoesNotExist:
            raise NotFound(detail="Le billet correspondant n'existe pas")
        except Participant.DoesNotExist:
            raise NotFound(detail="Pas de place pour ce billet")


class BilletSearch(ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = ParticipantSearchSerializer

    def get_queryset(self):
        search = self.kwargs['search']
        participants = Participant.objects.filter(Q(first_name__icontains=search) | Q(last_name__icontains=search))[:5]
        if participants:
            return participants
        else:
            raise Http404


@permission_classes([IsAuthenticated])
@api_view(['GET'])
def search(request, search):
    response = []
    participants = Participant.objects.filter(Q(first_name__icontains=search) | Q(last_name__icontains=search))[:5]
    nb_resultats = len(participants)
    for participant in participants:
        response.append(ParticipantSearchSerializer(participant).data)
    for i in range(nb_resultats, 5):
        response.append({
            'first_name': '',
            'last_name': '',
            'id': ''
        })
    return Response(response)


from api.serializers.admin import ClientSerializer


class ClientViewSet(viewsets.ModelViewSet):
    """
    permet de se créer des comptes dans l'espace client
            """
    serializer_class = ClientSerializer
    permission_classes = [AllowAny]
    api = AdhesionAPI()

    def get_queryset(self):
        queryset = None
        if self.request.user.is_authenticated:
            if self.request.user.has_perm(IsEventManager):
                queryset = Client.objects.all()
            elif self.request.user.client:
                queryset = Client.objects.filter(user=self.request.user)
        email = self.request.query_params.get('email', None)
        if email is not None:
            queryset = queryset.filter(email=email)
        return queryset

    @action(methods=['get'], detail=True)
    def orders(self, request, pk=None):
        client = self.get_queryset().get(id=pk)
        orders = []
        # Pour chaque commande
        print(client)
        for order in Order.objects.filter(client=client, status=Order.STATUS_VALIDATED,
                                          event__end_time__gt=timezone.now()).all():
            orders.append(order)
        return Response(OrderSerializer(orders, many=True).data)

    def create(self, request, *args, **kwargs):
        try:
            user = User.objects.get(email=request.data['email'])  # il existe déjà
            form = PasswordResetForm({'email': request.data['email']})
            if form.is_valid():
                form.save(request=request)
                try:
                    client = Client.objects.get(user=user)  # il a déjà un client; l'inverse n'est pas normal ....
                    open_events = 0
                    return Response(
                        "Vous possédez déjà un compte sur la billetterie, vous recevrez un mail pour définir votre mot de passe",
                        status=418)
                except Client.DoesNotExist:
                    try:
                        if (request.data['va']) and request.data['va'] != "" and request.data['va'] != None:
                            va_key = request.data['va']
                            if not self.api.tester_carte_va(va_key, request.data['first_name'],
                                                            request.data['last_name'],
                                                            request.data['email']):
                                return Response(
                                    "Votre carte VA semble invalide, merci de vérifier que votre adresse email est bien celle de votre compte VA",
                                    500)
                    except KeyError:
                        request.data['va'] = None
                    client = Client(user=user,
                                    last_name=request.data['last_name'],
                                    first_name=request.data['first_name'],
                                    email=request.data['email'],
                                    phone=request.data['phone'] if request.data['phone'] != "" else None,
                                    numero_VA=request.data.get('va', None)
                                    )
                    client.save()
                    if client.numero_VA and client.numero_VA != "":
                        client.va_validating = True
                        token = va_activation_token.make_token(client)
                        mail = VaValidationEmail(client, token, to=(client.email,))
                        mail.send(True)
                        return Response(ClientSerializer(client).data, 200)
                    return Response(ClientSerializer(client).data, 200)
            else:
                return Response("erreur", 505)
        except User.DoesNotExist:
            try:
                if (request.data['va']) and request.data['va'] != "" and request.data['va'] != None:
                    va_key = request.data['va']
                    if not self.api.tester_carte_va(va_key, request.data['first_name'], request.data['last_name'],
                                                    request.data['email']):
                        return Response(
                            "Votre carte VA semble invalide, merci de vérifier que votre adresse email est bien celle de votre compte VA",
                            500)
            except KeyError:
                request.data['va'] = None
            user = User.objects.create_user(username=request.data['email'], email=request.data['email'],
                                            password=request.data['password'])
            # user.last_name = request.data['last_name']
            # user.first_name = request.data['first_name']
            # user.is_active = False
            client = Client(user=user,
                            last_name=request.data['last_name'],
                            first_name=request.data['first_name'],
                            email=request.data['email'],
                            phone=request.data['phone'] if request.data['phone'] != "" else None,
                            numero_VA=request.data.get('va', None)
                            )
            client.save()
            if client.numero_VA and client.numero_VA != "":
                client.va_validating = True
                client.save()
                token = va_activation_token.make_token(client)
                mail = VaValidationEmail(client, token, to=(client.email,))
                mail.send(True)
                return Response(ClientSerializer(client).data, 200)
            # form = PasswordResetForm({'email': request.data['email']})
            # if form.is_valid():
            #    form.save(request=request)
            #    return Response("Vous revevrez un mail", 200)
            return Response(ClientSerializer(client).data, 200)

    def update(self, request: Request, *args: Any, **kwargs: Any):
        # print('salut')
        client = self.get_object()
        serializer = self.get_serializer(instance=client, data=request.data)
        old_va = client.numero_VA
        if serializer.is_valid(raise_exception=True):
            if client.user is not None:
                user = client.user  # on met à jour l'email/username de l'utilisateur associé au client, pour éviter d'avoir 2 infos différentes
                if user.email == user.username:
                    user.username = serializer.validated_data['email']
                    user.email = serializer.validated_data['email']
                # print(user)
                # print(user.username)
                user.save()
            client = serializer.save()
            if client.numero_VA is not None:
                try:
                    if not self.api.tester_carte_va(client.numero_VA, client.first_name, client.last_name, client.email,
                                                    client.phone):
                        client.numero_VA = None
                        client.va_validating = False
                except:
                    client.numero_VA = None
                    client.va_validating = False
                if client.numero_VA is not None and client.numero_VA != old_va:
                    client.va_validating = True
                    token = va_activation_token.make_token(client)
                    mail = VaValidationEmail(client, token, to=(client.email,))
                    mail.send(True)
                client.save()
            else:
                client.va_validating = False
                client.save()
            return Response(ClientSerializer(client).data)
    @action(detail=False, methods=['post'])
    def getVA(self, request: Request, *args: Any, **kwargs: Any):
        if request.data['va'] and request.data['va'] != "" and request.data['va'] != None:
            va_key = request.data['va']
            try:
                member = self.api.get_member_from_card(va_key)
                return Response({"first_name": member['first_name'], "last_name": member['last_name'],"email": member['email'],"phone": member['phone'], "numero_VA": va_key}, 200)
            except (AssertionError, ValueError) as e:
                return Response({"error": "va_card_invalid"}, 400)

        else:
            return Response({"error": "va_card_invalid"}, 400)

    @action(detail=False, methods=['post'])
    def activateva(self, request: Request, *args: Any, **kwargs: Any):
        if request.data['token'] is None or request.data['uidb64'] is None:
            return Response('Activation link is invalid!', status=401)
        token = request.data['token']
        uidb64 = request.data['uidb64']
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            client = Client.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, Client.DoesNotExist):
            client = None
        print(client)
        if client is not None and va_activation_token.check_token(client, token):
            client.va_validating = False
            user = client.user
            client.save()
            payload = jwt_payload_handler(user)
            jwt_token = jwt_encode_handler(payload)

            return Response({
                'jwt': jwt_token,
                'client': ClientSerializer(client).data
            }, status=status.HTTP_202_ACCEPTED)
            # Now we force the user to change his password
            # return redirect(login_after_password_change)

            # return Response('Thank you for your email confirmation. Now you can login your account.')
            # return redirect('account_change_password2')
        else:
            return Response('Activation link is invalid!')



from api.serializers.public.serializers import InvitationRAIDSerializer, InvitationGrantRAIDSerializer
from api.models import InvitationGrant


class InvitationRAID(viewsets.ModelViewSet):
    """
    Renvoie une liste d'invitation qu'on peut filtrer avec deux paramètres GET:
    client=id
    event=id
    /invitation/?event=1&client=2
    """
    serializer_class = InvitationRAIDSerializer
    permission_classes = [IsEventManager]

    def get_queryset(self):
        queryset = Invitation.objects.all()
        event = self.request.query_params.get('event', None)
        if event is not None:
            queryset = queryset.filter(event__id=event)
        client = self.request.query_params.get('client', None)
        if client is not None:
            queryset = queryset.filter(client=client)
        return queryset


@api_view(http_method_names=['GET'])
@permission_classes([IsEventManager])
def send_mail(request, pk):
    invitation = Invitation.objects.get(pk=pk)
    invitation.send_email()
    invitation.save()
    return Response(pk)


class ProductsByEvent(viewsets.ReadOnlyModelViewSet):
    """
    Retourne la liste de tous les produits, qu'on peut filter avec un paramètre GET 'event=id
    ex: /events-products/?event=1
    On peut avoir les produits des évènements sur lesquels l'utilisateur a un Membership/Contrôle d'accès avec /events-products/?event-filter=membership
    """
    serializer_class = ProductSerializer
    permission_classes = [IsEventManager]

    def get_queryset(self):
        queryset = Product.objects.all()
        event = self.request.query_params.get('event', None)
        if event is not None:
            queryset = queryset.filter(event__id=event)
        filter = self.request.query_params.get('event-filter', None)
        if filter == 'membership' and self.request.user.membership:
            if self.request.user.membership.events:
                queryset = queryset.filter(event__in=self.request.user.membership.events.all())
        return queryset


class GrantViewSet(viewsets.ModelViewSet):
    queryset = InvitationGrant.objects.all()
    serializer_class = InvitationGrantRAIDSerializer
    permission_classes = [IsEventManager]


class APIOrderViewSet(viewsets.ModelViewSet):
    """
    Récupère une commande par son ID
    endpoint pour la communication avec Dossieraid
    on peut rechercher avec des paramètres GET event et client
    """
    serializer_class = OrderSerializer

    # permission_classes = [IsEventManager]
    def get_queryset(self):
        queryset = Order.objects.all()
        event = self.request.query_params.get('event', None)
        if event is not None:
            queryset = queryset.filter(event__id=event)
        client = self.request.query_params.get('client', None)
        if client is not None:
            queryset = queryset.filter(client__id=client)
        return queryset


class UserView(viewsets.ModelViewSet):
    """

    """
    serializer_class = FullUserView
    model = User
    permission_classes = [AllowAny]

    def get_queryset(self):
        if self.request.user.is_anonymous:
            return None
        if self.request.user.is_staff:
            return User.objects.all()
        else:
            return User.objects.filter(id=self.request.user.id)


class ClientRAIDViewSet(viewsets.ModelViewSet):
    """
            cherche un Client par son email depuis un paramètre GET
            ex: /api/client/?email=test@example.com
            """
    serializer_class = ClientSerializer
    permission_classes = [IsEventManager]

    def get_queryset(self):
        queryset = Client.objects.all()
        email = self.request.query_params.get('email', None)
        if email is not None:
            queryset = queryset.filter(email=email)
        return queryset


class ParticipantImageUpload(viewsets.ViewSet):
    parser_classes = [MultiPartParser]
    permission_classes = [IsAuthenticated]

    @action(methods=['put', 'post'], detail=True)
    def upload(self, request, pk):
        print(request.data['file'])
        try:
            participant: Participant = Participant.objects.get(id=pk)
            if participant.billet.order.client.user.id == request.user.id:
                file = request.data['file']
                participant.avatar = file
                participant.save()
                return Response(status=status.HTTP_204_NO_CONTENT)
            else:
                raise PermissionError("mauvais participant")
        except Exception as e:
            print(type(e))
            print(e.args)
            return Response(status=status.HTTP_400_BAD_REQUEST)


def access_upload(request, file):
    path = f'uploads/{file}'
    if request.user.is_staff and ".." not in file and "/" not in file:  # Prevent injections (somewhat)
        if not exists(path):
            raise Http404

        image = open(path, 'rb')
        return FileResponse(image)
    else:
        return HttpResponseForbidden('Not authorized')

