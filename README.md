# Billevent API

Billevent is the online ticket office manager for the BdE INSA Lyon student
union. It allows to sells tickets for events, accept payments, issue and
control tickets.

## Technologies

The API is driven by the Django REST Framework and frontend applications
connects to it directly (eventually with authentication tokens)

The database used on the project is PostgreSQL but on a local developer
computer it can fit with a SQLite DB.

When application is ready, it is deployed using Docker on a pre-prod and,
if code and tests (no one written currently) are OK, then deployed on
production. It's a CI/CD scheme managed by GitLab.

## Setup

### Side services

- A database service like PostgreSQL or SQLite (never tried for MySQL)
- A broker message service like RabbitMQ

### Environment Variables

```dotenv
API_URL=https://your.api.domain.fr
BROKER_URL=amqp://guest:guest@rabbitmq-server:5672/
DATABASE_URL=postgres://postgres@postgres-server/billevent
DEFAULT_FROM_EMAIL=BdE INSA Lyon <billetterie@bde-insa-lyon.fr>
DOMAINS=your.api.domain.fr,your.frontend.domain.fr,your.manager.domain.fr
ENV=development
DEBUG=False
FORWARDED_ALLOW_IPS=*
FRONTEND_URL=https://your.frontend.domain.fr
HOST=localhost
MAILGUN_API_KEY=key-YOURAPIKEY
MAILGUN_DOMAIN=your.api.domain.fr
MERCANET_INTERFACE_VERSION=IR_WS_2.18
MERCANET_KEY_VERSION=1
MERCANET_MERCHANT_ID=<the merchant id>
MERCANET_SECRET_KEY=<the secret key>
MERCANET_REPONSE_AUTO_URL=https://your.api.domain.fr/pay/auto/
MERCANET_URL=https://payment-webinit-mercanet.test.sips-atos.com/rs-services/v2/paymentInit
ADHESION_URL=https://....com
ADHESION_CLIENT_ID
ADHESION_CLIENT_SECRET
CLEAN_ORDERS_CRON=5 * * * * # le cron pour la suppression des commandes invalides
```

### Simple launch (development only)

Write all environment variables to a `.env` file on the root of project

Then simply run `python manage.py runserver`

## Testing Mercanet locally
This server uses the BNP Paribas API Mercanet to process payements. The only thing is, the bank API needs to be able to contact our local development server, which is impossible running on localhost.

One way to make it testable is to use a tool to receive webhooks on localhost, such as [Ultrahook](https://www.ultrahook.com/). After generating an API key and downloading the binary on your computer, you can simply do the binding by running the command :
```bash
ultrahook mercanet 8000
```
which will yield an output similar to this one : 
```bash
Authenticated as billetterie
Forwarding activated...
https://billetterie-mercanet.ultrahook.com -> http://localhost:8000
```

It means the Mercanet API server can access our local API though the address `https://billetterie-mercanet.ultrahook.com`
Change accordingly the `URL` parameter in the `REPONSE_AUTO_URL` dictionary in the `BILLEVENT` constant in `./billetterie/settings.py` or the envvar `MERCANET_REPONSE_AUTO_URL`.

You can now test payments !

# Intégration avec dossieraid
Il faut créer un compte root pour dossieraid, ses endpoints sont en permission 'IsEventManager'

# Vue d'ensemble
Voir le [wiki](https://gitlab.com/sia-insa-lyon/BdEINSALyon/billevent/api/-/wikis/home) sur l'interaction entre les différents services. Le manager ne devrait plus être utilisé, notamment
car le framework graphique a trop changé pour pouvoir retrouver la documentation.

## Interface Orga quick-n-dirty
accessible à l'url `/orga/`, elle permet aux Orgas d'accéder à seulement ce qu'il leur faut.
On met les orgas *is_staff* et *is_superuser*.

Il faut mettre les **orgas soft** en *is_staff* et leur donnes les permissions nécessaires avec le système habituel Django (can view ...)

# Fonctionnalité permanencier
Les permanenciers doivent avoir un Contrôle d'accès (Membership) qui porte sur un seul évènement.
Sinon l'affichage rapide des places restantes ne marchera pas.
Beaucoup de pages ne marcheront pas si l'utilisateur n'ouvre pas de permanence
l'affichage des places restantes montre le nombre de places restantes du premier produit du premier évènement du permanencier

# Gala
## Fonctionnement de la liste d'attente
Il y a des produits style "Soirée + liste d'attente Repas" qui sont en vente.
Il faut également des produits de ré-équilibrage en mode de vente 'Sur InvitationGrant uniquement' avec 0 seats (pour ne pas interférer avec les places prises avant).
Les produits de liste d'attente indiquent dans une champ à quels produits sur liste d'attente ils donnent accès.
(ex: Soirée+liste d'attente Repas indique qu'il donne accès à Ré-équilibrage Repas-Soirée) 
Quand les places se libèrent, les orgas séléctionnent les billets à "échanger" et le système ajoute des InvitationGrant
aux clients pour les produits en liste d'attente

```bash
python manage.py dumpdata --exclude auth.permission --exclude contenttypes --exclude auth.user --exclude api.membership --exclude sessions.session --exclude admin.logentry --exclude api.client --exclude api.invitation --exclude api.invitationgrant> setup_produits.json
```

## Licence
This project is under GNU AGPL 3.0 License

### Contributors
```
Philippe VIENNE <philippegeek@gmail.com>
Alban PRATS
Jean RIBES
Hugo REYMOND
François LALLEVE
Gabriel AUGENDRE
```
