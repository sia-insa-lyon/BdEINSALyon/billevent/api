# À exécuter dans la ligne de commande python
# python manage.py shell
# et là on colle dans la console

from statistics import *

from api.models import *

[str(event.id) + event.name for event in Event.objects.all()]
orders = Order.objects.filter(event=4, status=Order.STATUS_VALIDATED)
amounts = []
places = []
for order in orders:
    if order.transaction is None:
        print(order)
    elif order.transaction.mercanet is None:
        print(order)
    else:
        amounts.append(order.amount)
    place = 0
    for b in order.billets.all():
        billet:Billet=b
        participants = billet.participants.all().count()
        if participants not in [0,1,2]:
            print(order)
            print(billet)
        place+=participants
    places.append(place)

def recap_stats(iterable):
    return "min: {}, max:{} mean: {}, median: {}, variance: {}, stddev: {}".format(min(iterable),max(iterable),mean(iterable),median(iterable),pvariance(iterable),stdev(iterable))

print("coûts des commandes "+recap_stats(amounts))
print("nombres de personnes dans une commande "+recap_stats(places))


