from django.contrib import admin
from .models import *


@admin.register(TransactionRequest, MercanetToken)
class BasicAdmin(admin.ModelAdmin):
    pass



@admin.register(TransactionMercanet)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ['id', 'responseText', 'transactionReference', 'amount', 'transactionDateTime']
    list_filter = ['responseText', 'transactionDateTime']
    search_fields = ['transactionReference', 'maskedPan']
    exclude = ['customerIpAddress', 'customerMobilePhone', 'cardProductCode', 'issuerCode']
    readonly_fields = ['status']