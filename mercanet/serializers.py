from collections import OrderedDict

from rest_framework import serializers
from mercanet.models import TransactionMercanet
class TransactionMercanetSerializer(serializers.ModelSerializer):#pour le 1er enregistrement, avant paiement
    class Meta:
        model = TransactionMercanet
        fields= '__all__'
    def validate(self, attrs):
        modified = OrderedDict()
        for k,v in attrs.items():
            if v=="null":modified[k]=0 #remplace tous les "null" qui ne sont pas pris par DRF en 0
            else: modified[k]=v
        return super().validate(modified)

