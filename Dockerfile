FROM python:3.6

EXPOSE 8000

RUN apt-get update && apt-get install -y libpq-dev python3-dev gcc g++ libxslt-dev libtiff5-dev libjpeg-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk && apt-get install cron --no-install-recommends -y
ENV LIBRARY_PATH=/lib:/usr/lib

WORKDIR /app

COPY requirements.txt /app
RUN pip3 install -r requirements.txt && pip3 install psycopg2==2.8.6
COPY . /app
VOLUME /app/staticfiles
ENV DATABASE_URL postgres://user:password@server:5432/database

RUN chmod +x bash/run-prod.sh
RUN chmod +x bash/run-cron.sh
CMD /app/bash/run-prod.sh
