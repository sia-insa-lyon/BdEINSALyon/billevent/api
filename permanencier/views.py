import logging
from typing import List

from django.core.exceptions import MultipleObjectsReturned
from django.db import transaction
from django.db.models import Sum, Count, QuerySet, Q
from django.db.models.functions import TruncDay, TruncHour
from django.http import HttpResponseRedirect

# Create your views here.
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.http import urlencode
from django.views.generic import CreateView, ListView, FormView, UpdateView, DetailView, DeleteView
from django.views.generic.base import TemplateView, View, RedirectView

from api.adhesion_api import AdhesionAPI, AdhesionException
from api.models import Option, Stand, Permanence, Transaction, VA, PricingRule, Compostage, File, PaymentMethod, \
    composter
from billetterie import settings
from permanencier.error_handler import display_error, PermanencierException
from permanencier.models import Client, Order, Billet, Product, Participant
from permanencier.forms import ClientForm, ClientSearchForm, OrderForm, BilletForm, ParticipantForm, \
    TransactionPermanencierForm, PermanenceOpenForm, VAForm, AdhesionMemberForm, QuickAchatForm, QuickAchatVAForm
from django.contrib.auth.models import User
from permanencier.utils import PermanencierMixin, permission_permanencier, PermissionPermanencierMixin, permission_orga

logger = logging.getLogger('permanencier')


@method_decorator(transaction.atomic, name='dispatch')
class ClientCreateView(PermanencierMixin, CreateView):
    form_class = ClientForm
    template_name = 'client/create.html'

    def form_valid(self, form):
        client = form.save(commit=False)
        try:
            user = User.objects.get(email=client.email)
            print(user.email)
        except User.DoesNotExist:
            user = client.make_user()
            user.save()
            print(user)
        client.user = user

        assert user is not None
        client.save()
        logger.info("nouveau client : {}".format(client))
        return HttpResponseRedirect(reverse('order-create', kwargs={'client_id': client.pk}))
    # @transaction.atomic()
    # def dispatch(self, request, *args, **kwargs):
    #    return super().dispatch(request, *args, **kwargs)


class ClientChangeView(PermanencierMixin, UpdateView):
    model = Client
    form_class = ClientForm
    template_name = 'client/edit.html'

    def get_success_url(self):
        return reverse('order-create', kwargs={'client_id': self.get_object().pk})


class ClientSearchView(PermanencierMixin, ListView, FormView):
    template_name = 'client/search.html'
    form_class = ClientSearchForm

    def get_success_url(self):
        return reverse('client-search')

    def get_queryset(self):
        queryset = Client.objects.all()  # filter(first_name__startswith='WEI')
        email = self.request.GET.get('email', None)
        first_name = self.request.GET.get('first_name', None)
        last_name = self.request.GET.get('last_name', None)

        q1 = q2 = q3 = queryset.filter(id=0)  # pour avoir un queryset vide
        if email is not None and email != '':
            q1 = queryset.filter(email__contains=email)
        if first_name is not None and first_name != '':
            q2 = queryset.filter(first_name__contains=first_name)
        if last_name is not None and last_name != '':
            q3 = queryset.filter(last_name__contains=last_name)
        result = q1 | q2 | q3
        return result

    def get_context_data(self, **kwargs):
        kwargs['va_form'] = VAForm()
        kwargs['adherent_search_form'] = AdhesionMemberForm()
        client_id = self.request.session.get('client_id', None)
        billet_id = self.request.session.get('billet_id', None)
        if client_id is not None:
            kwargs['nouveau_client'] = Client.objects.get(id=client_id)
        if billet_id is not None:
            kwargs['ancien_billet'] = Billet.objects.get(id=billet_id)
        return super().get_context_data(**kwargs)


class OrderCreateView(PermanencierMixin, RedirectView):
    """
    Redirige l'orga instantanément sur une commande pour l'évènement qu'il dirige
    """

    def get_redirect_url(self, *args, **kwargs):
        perm = Permanence.from_user(self.request.user)
        assert perm is not None, "Vous n'avez pas de permanence ouverte"
        event = perm.event
        assert event is not None, "Aucun évènement n'est associé à votre permanence"
        client = Client.objects.get(id=self.kwargs['client_id'])
        order = Order(event=event, client=client, status=Order.STATUS_SELECT_PRODUCT)
        order.save()
        return reverse('billet-create', kwargs={'order_id': order.pk})


class OrderView(PermissionPermanencierMixin, DetailView):
    queryset = Order.objects.all()
    template_name = 'order/view.html'

    def get_context_data(self, **kwargs):
        kwargs['client'] = self.get_object().client
        kwargs['order'] = self.get_object()
        return super().get_context_data(**kwargs)


@method_decorator(transaction.atomic, name='dispatch')
class BilletCreateView(PermanencierMixin, CreateView):
    model = Billet
    template_name = 'billet/create.html'
    form_class = BilletForm

    def get_order(self):
        return Order.objects.get(id=self.kwargs['order_id'])

    def get_form(self, form_class=None):
        form = super().get_form()
        order = self.get_order()
        form.fields["product"].queryset = Product.objects.filter(event=self.get_order().event).exclude(
            rules__type=PricingRule.TYPE_VA_PERMANENCIER)
        if VA.objects.filter(
                VA=order.client.numero_VA).count() == 0 and order.client.numero_VA is not None and order.client.numero_VA != '':
            form.fields["product"].queryset = Product.objects.filter(event=self.get_order().event)
        # form.fields["options"].queryset = Option.objects.filter(event=self.get_order().event)
        return form

    def get_initial(self):
        order = self.get_order()
        try:
            if VA.objects.filter(VA=order.client.numero_VA).count() == 0:
                return {'product': order.event.products.filter(rules__type=PricingRule.TYPE_VA_PERMANENCIER).first().pk}
            else:
                return {'product': order.event.products.exclude(rules__type=PricingRule.TYPE_VA_PERMANENCIER).last().pk}
        except AttributeError:
            return None

    def form_valid(self, form):
        billet = form.save(commit=False)
        billet.order = self.get_order()
        billet.save()
        if billet.order.is_valid(permanencier=True):  # le mode 'permanencier désactive la vérification VA !
            billet.order.status = Order.STATUS_PAYMENT
            if billet.product.rules.filter(type=PricingRule.TYPE_VA_PERMANENCIER).count() > 0:
                VA(event=billet.order.event, VA=billet.order.client.numero_VA).save()
            billet.order.save()
            self.success_url = reverse('participant-create',
                                       kwargs={'order_id': billet.order.pk, 'billet_id': billet.pk})
            logger.info("{} crée pour le client {} sur {}".format(billet, billet.order.client, billet.product))
            return HttpResponseRedirect(self.success_url)
        else:
            billet.delete()
            return display_error(self.request,
                                 "Billet invalide ! Les jauges de l'évènement sont atteintes ou la carte VA a déjà été utilisée",
                                 self.request.get_raw_uri())

    def get_context_data(self, **kwargs):
        kwargs['order'] = self.get_order()
        kwargs['client'] = self.get_order().client
        return super().get_context_data(**kwargs)


@method_decorator(transaction.atomic, name='dispatch')
class BilletChangeView(PermanencierMixin, UpdateView):
    queryset = Billet.objects.filter(canceled=False, refunded=False)
    template_name = 'billet/edit.html'
    form_class = BilletForm

    def get_order(self):
        return Order.objects.get(id=self.kwargs['order_id'])

    def get_form(self, form_class=None):
        form = super().get_form()
        form.fields["product"].queryset = Product.objects.filter(event=self.get_order().event).exclude(
            rules__type=PricingRule.TYPE_VA_PERMANENCIER)
        # form.fields["options"].queryset = Option.objects.filter(event=self.get_order().event)
        return form

    def form_valid(self, form):
        billet = form.save(commit=False)
        if billet.order.is_valid(permanencier=True):  # le mode 'permanencier désactive la vérification VA !
            billet.order.status = Order.STATUS_PAYMENT
            billet.order.save()
            billet.save()
            if billet.participants.all().count() > 0:
                return HttpResponseRedirect(reverse('participant-edit',
                                                    kwargs={'order_id': billet.order.pk, 'billet_id': billet.pk,
                                                            'pk': billet.participants.all().first().pk}))
            else:
                self.success_url = reverse('participant-create',
                                           kwargs={'order_id': billet.order.pk, 'billet_id': billet.pk})
            return HttpResponseRedirect(self.success_url)
        else:
            billet.delete()
            return display_error(self.request,
                                 "Billet invalide ! Les jauges de l'évènement sont atteintes ou la carte VA a déjà été utilisée",
                                 reverse('order-view', kwargs={'pk': self.get_order().pk}))

    def get_context_data(self, **kwargs):
        kwargs['order'] = self.get_order()
        kwargs['client'] = self.get_order().client
        return super().get_context_data(**kwargs)


class ParticipantCreateView(PermanencierMixin, CreateView):
    form_class = ParticipantForm
    template_name = 'participant/create.html'

    def form_valid(self, form):
        participant = form.save(commit=False)
        participant.billet = Billet.objects.get(id=self.kwargs['billet_id'])
        participant.save()
        logger.info("Participant {} ajouté au {}".format(participant, participant.billet))
        return HttpResponseRedirect(reverse("transaction-create", kwargs={'order_id': participant.billet.order.id}))

    def get_initial(self):
        client = Order.objects.get(id=self.kwargs["order_id"]).client
        return {
            'first_name': client.first_name,
            'last_name': client.last_name,
            'email': client.email,
            'phone': client.phone,
            'billet': self.kwargs['billet_id'],
            # 'billet': Billet.objects.get(id=self.kwargs['billet_id'])
        }

    def get_context_data(self, **kwargs):
        order = Order.objects.get(id=self.kwargs["order_id"])
        kwargs['order'] = order
        kwargs['client'] = order.client
        return super().get_context_data(**kwargs)


class ParticipantChangeView(PermanencierMixin, UpdateView):
    queryset = Participant.objects.all()
    form_class = ParticipantForm
    template_name = 'participant/edit.html'

    def get_success_url(self):
        return reverse('transaction-create', kwargs={'order_id': self.get_object().billet.order.id})

    def get_context_data(self, **kwargs):
        order = Order.objects.get(id=self.kwargs["order_id"])
        kwargs['order'] = order
        kwargs['client'] = order.client
        return super().get_context_data(**kwargs)


@method_decorator(transaction.atomic, name='dispatch')
class TransactionCreateView(PermanencierMixin, CreateView):
    template_name = 'transaction/create.html'
    form_class = TransactionPermanencierForm

    def get(self, request, *args, **kwargs):
        order = Order.objects.get(id=self.kwargs['order_id'])
        try:
            assert order.is_valid(permanencier=True), "Commande non valide"
            return super().get(request, *args, **kwargs)
        except AssertionError:
            return display_error(self.request,
                                 "La commande n'est pas valide ! Le client a peut-être déjà des billets au tarif VA, ou alors il n'y a plus de places pour l'évènement."
                                 "Contactez votre responsable", reverse('order-view', kwargs={'pk': order.pk}))

    def get_context_data(self, **kwargs):
        order = Order.objects.get(id=self.kwargs['order_id'])
        kwargs['order'] = order
        kwargs['client'] = order.client
        assert order.is_valid(permanencier=True), "Commande non valide"
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        order = Order.objects.get(id=self.kwargs['order_id'])
        try:
            assert order.is_valid(
                permanencier=True), "Commande non valide"  # normalement ça arrive que si on modifie les billets après coup
            transaction = form.save(commit=False)
            transaction.order = order
            transaction.permanence = Permanence.from_user(self.request.user)
            transaction.amount = transaction.order.amount
            transaction.save()
            order.status = Order.STATUS_VALIDATED
            order.save()
            order.send_tickets()
            logger.info("{} payée par {} avec {}".format(order, self.request.user, transaction))
            return HttpResponseRedirect(reverse("order-view", kwargs={'pk': order.pk}))
        except AssertionError:
            return display_error(self.request,
                                 "La commande n'est pas valide ! Le client a peut-être déjà des billets au tarif VA, ou alors il n'y a plus de places pour l'évènement\n."
                                 "Contactez votre responsable", reverse('order-view', kwargs={'pk': order.pk}))


class TransactionView(PermissionPermanencierMixin, DetailView):
    template_name = 'transaction/create.html'
    queryset = Transaction.objects.all()


class ErrorView(PermanencierMixin, TemplateView):
    template_name = 'error.html'

    def get_context_data(self, **kwargs):
        safe_url = self.request.GET.get('next', None)
        if safe_url is not None:
            test: bool = (safe_url.startswith('/') or safe_url.startswith(settings.BILLEVENT['API_URL']))
            assert test, "URL dangereuse"
        return {
            'msg': self.request.GET.get('msg', "Erreur inconnue"),
            'next': safe_url
        }


@permission_permanencier
def delete_billet(request, order_id, billet_id):
    billet = Billet.objects.get(id=billet_id)
    logger.info("{} a supprimé un billet {} pour {}".format(request.user, billet.product, billet.order.client))
    billet.delete()
    return HttpResponseRedirect(reverse('order-view', kwargs={'pk': order_id}))


class PermanenceCreateView(PermissionPermanencierMixin, CreateView):
    model = Permanence
    template_name = 'permanence/create.html'
    form_class = PermanenceOpenForm
    success_url = '../'

    def get_initial(self):
        return {  # le tag 'permanence' est rajouté par 'PermanencierMixin'
            'event': self.request.user.membership.events.all().first(),
            'permanencier': self.request.user.membership,
        }
    def get_form(self, form_class=None):
        form = super().get_form()
        form.fields["file"].queryset = File.objects.filter(event=self.request.user.membership.events.first(), active=True)
        return form

    def form_valid(self, form):
        perm = form.save(commit=False)
        perm.permanencier = self.request.user.membership
        perm.save()
        logger.info("permanence {} ouverte".format(perm))
        return HttpResponseRedirect(reverse('home'))


@permission_permanencier
def finir_ma_permanence(request):
    permanence = Permanence.from_user(request.user)
    permanence.closed_at = timezone.now()
    permanence.save()
    logger.info("permanence {} fermée".format(permanence))
    return HttpResponseRedirect(reverse('permanence-view', kwargs={'pk': permanence.pk}))


class HomeView(PermissionPermanencierMixin, TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        ventes = Transaction.objects.filter(order__event=self.request.user.membership.events.first())
        compostages = Compostage.objects.filter(billet__order__event=self.request.user.membership.events.first())
        kwargs['ventes'] = ventes.annotate(hour=TruncHour('date')).values('hour').annotate(total=Sum('amount')).order_by('hour')
        kwargs['compostages'] = compostages.annotate(hour=TruncHour('date')).values('hour').annotate(total=Count('id')).order_by('hour')
        return super().get_context_data(**kwargs)


class PermanencesFiniesListView(PermissionPermanencierMixin, ListView):
    template_name = 'permanence/list.html'
    context_object_name = 'permanences'

    def get_queryset(self):
        return Permanence.objects.filter(created_at__lt=timezone.now(), closed_at__lt=timezone.now())


class PermanenceView(PermissionPermanencierMixin, DetailView):
    queryset = Permanence.objects.all()
    template_name = 'permanence/view.html'

    def get_context_data(self, **kwargs):
        perm: Permanence = self.get_object()
        kwargs['count'] = perm.transactions.count()
        kwargs.update(perm.transactions.aggregate(total=Sum('amount')))  # aggregate renvoie {'total':Decimal(xx)}
        produits = Billet.objects.filter(order__transaction_guichet__permanence=perm).values('product').annotate(
            total=Count(
                'id'))  # le nombre de billets par produits ayant été payés avec des transaction faites lors de cette permanence
        kwargs['comptes'] = []
        for a in produits:
            p = Product.objects.get(id=int(a['product']))
            t = a['total']
            kwargs['comptes'].append((p, t, t * p.price_ttc))
        # options_billet = Billet.objects.filter(order__transaction_guichet__permanence=perm)
        # q = QuerySet(model=Option)
        # for b in options_billet:
        #    options = b.options.annotate(total=Count('id'))
        #    q = q | options
        # print(q)
        # print(q.annotate(total=Count('name')))
        return super().get_context_data(**kwargs)


@transaction.atomic
@permission_permanencier
def client_from_va(request):
    if request.method == 'POST':
        form = VAForm(request.POST)
        if form.is_valid():
            api = AdhesionAPI()
            try:
                member = api.get_member_from_card(form.data['code'])
            except ValueError:
                return display_error(request, "Numéro de carte VA incorrect", reverse('client-search'))
            except AssertionError:
                return display_error(request, "Erreur dans la définition de l'adhérent, contactez un admin",
                                     reverse('client-search'))

            queryset = Client.objects.filter(
                Q(email=member['email']) | Q(first_name=member['first_name']) | Q(last_name=member['last_name']) | Q(
                    numero_VA=form.data['code']))
            if queryset.count() > 0:
                if queryset.count() == 1:
                    client: Client = queryset.first()
                    client.numero_VA = form.data['code']
                    client.id_adhesion = member['id']
                    client.save()
                    return HttpResponseRedirect(reverse('client-edit', kwargs={'pk': queryset.first().pk}))
                else:
                    print(member)
                    return HttpResponseRedirect(reverse('client-search') + '?' + urlencode({
                        'first_name': member['first_name'],
                        'last_name': member['last_name'],
                        'email': member['email']
                    }))
            else:
                try:
                    user = User.objects.get(email=member['email'])
                except User.DoesNotExist:
                    user = User(first_name=member['first_name'],
                                last_name=member['last_name'],
                                email=member['email'],
                                username=member['email'])
                user.save()
                client = Client(first_name=member['first_name'],
                                last_name=member['last_name'],
                                phone=member['phone'],
                                email=member['email'])
                client.user = user
                client.numero_VA = form.data['code']
                client.id_adhesion = member['id']
                client.save()
                return HttpResponseRedirect(reverse('client-edit', kwargs={'pk': client.pk}))
    return display_error(request, "Recherche VA échouée, essayez à la main", reverse('client-search'))


@permission_orga
@transaction.atomic  # il faut éviter une race condition où quelqu'un achète un billet pendant qu'un orga effectue un remboursement
def annuler_et_recreer_billets(request, old_billet_id, acheteur_client_id):
    try:
        acheteur_client = Client.objects.get(id=request.session['client_id'])
        old_billet = Billet.objects.get(id=request.session['billet_id'])
        if request.method == 'GET':
            context = {
                'nouveau_client': acheteur_client,
                'ancien_client': old_billet.order.client,
                'ancien_billet': old_billet,
            }
            return render(request, 'permanence/remboursement.html', context=context)

        if request.method == 'POST':
            logger.info("Échange de {} à {}".format(old_billet, acheteur_client))
            assert old_billet_id == request.session['billet_id'], "Incohérence"
            assert acheteur_client_id == request.session['client_id'], "Incohérence"
            to_cancel = Billet.objects.get(pk=old_billet_id)
            racheteur = Client.objects.get(pk=acheteur_client_id)
            if to_cancel.product.rules.filter(type=PricingRule.TYPE_VA_PERMANENCIER).count() > 0:
                old_va = VA.objects.get(VA=to_cancel.order.client.numero_VA)
                old_va.delete()
            to_cancel.canceled = True
            to_cancel.refunded = True
            to_cancel.save()
            new_order = Order(client=racheteur, event=to_cancel.order.event)
            new_order.save()
            new_product = new_order.event.products.exclude(
                rules__type=PricingRule.TYPE_VA_PERMANENCIER).first()  # on commence avec un produit normal
            if acheteur_client.numero_VA is not None:
                if VA.objects.filter(VA=acheteur_client.numero_VA).count() == 0:  # si le client a droit à un produit VA
                    new_product = new_order.event.products.filter(rules__type=PricingRule.TYPE_VA_PERMANENCIER).first()
                    VA(VA=acheteur_client.numero_VA, event=new_order.event).save()
            new_billet = Billet(product=new_product, order=new_order)
            new_billet.save()
            cancel_selection_billet(request)
            cancel_selection_client(request)
            return HttpResponseRedirect(reverse('billet-edit', kwargs={'pk': new_billet.pk, 'order_id': new_order.pk}))
    # dans le cas où l'orga n'a pas marqué de client et billet à supprimer
    except (Client.DoesNotExist, Billet.DoesNotExist, KeyError):
        display_error(request,
                      "Veuillez marquer un billet à rembourser et un nouveau client. Créez le nouveau client s'il n'existe pas encore",
                      reverse('client-search'))


@permission_permanencier
def select_client(request, pk: int):
    request.session['client_id'] = pk
    return HttpResponseRedirect(reverse('client-search'))


@permission_permanencier
def select_billet(request, pk: int):
    request.session['billet_id'] = pk
    # if request.session.get('billet_id', None) is not None and request.session.get('client_id', None) is not None:
    #    return HttpResponseRedirect(reverse('billets-refund-recreate',
    #                                        kwargs={'old_billet_id': request.session['billet_id'],
    #                                                'acheteur_client_id': request.session['client_id']}))
    return HttpResponseRedirect(reverse('client-search'))


@permission_permanencier
def cancel_selection_client(request):
    request.session['client_id'] = None
    return HttpResponseRedirect(reverse('client-search'))


@permission_permanencier
def cancel_selection_billet(request):
    request.session['billet_id'] = None
    return HttpResponseRedirect(reverse('client-search'))


class CommandesOrphelinesListView(PermissionPermanencierMixin, ListView):
    template_name = 'order/list.html'

    def get_queryset(self):
        return Order.objects.filter(event=self.request.user.membership.events.first(), transaction=None,
                                    transaction_guichet=None)


@permission_orga
def delete_order(request, pk):
    order = Order.objects.get(pk=pk)
    logger.warning("Suppression de la {} ({}) par {}. ".format(order, order.amount, request.user))
    client = order.client
    success_url = reverse('client-edit', kwargs={'pk': client.pk})
    if order.transaction_guichet is None:  # pas de transaction faite
        assert order.transaction is None
        order.delete()
        return HttpResponseRedirect(success_url)
    else:
        return display_error(request, "Cette commande a été payée, la suppression n'est donc pas autorisée",
                             success_url)


class ClientListView(PermissionPermanencierMixin, ListView):
    queryset = Client.objects.all()
    template_name = 'client/list.html'


class AdherentSearchView(PermanencierMixin, FormView):
    form_class = AdhesionMemberForm
    template_name = 'client/va-search-form.html'
    api = AdhesionAPI()

    def form_valid(self, form):
        for k, v in list(form.cleaned_data.items()):
            if v is None:  # pour éviter de mettre '&birthday=None dans l'url adhésion
                form.cleaned_data.pop(k)
        params = urlencode(form.cleaned_data)
        try:
            members: List = self.api.search_member(params)
        except AdhesionException as e:
            return display_error(self.request, '; '.join(e.args), reverse('client-search'))
        for member in members:
            member['form'] = VAForm(initial={'code': member['card']['code']}) if member['card'] is not None else ''
        return render(self.request, template_name='client/adherent-list.html', context={'adherents': members})



class QuickAchatView(PermanencierMixin, FormView):
    form_class = QuickAchatForm
    template_name = 'permanence/quick-nonva.html'

    @method_decorator(transaction.atomic)  # pour éviter de niquer trop la DB si y'a une erreur
    def form_valid(self, form):
        try:
            user = User.objects.get(email=form.data['email'])
        except User.DoesNotExist:
            try:
                user = User.objects.get(username=form.data['email'])
            except User.DoesNotExist:
                user = User.objects.create(first_name=form.data['prenom'],
                                   last_name=form.data['nom'],
                                   email=form.data['email'],
                                   username=form.data['email']
                                   )
        try:
            client = Client.objects.get(user=user) #avec user.client  je n'arrive pas à catch l'exception
        except Client.DoesNotExist:
            client = Client.objects.create(first_name=form.data['prenom'],
                                       last_name=form.data['nom'],
                                       email=form.data['email'],
                                       phone=form.data['phone'],
                                       user=user)
        order = Order.objects.create(client=client, event=self.request.user.membership.events.first(), status=Order.STATUS_PAYMENT)
        produit_non_va = Product.objects.filter(event=order.event).exclude(
            rules__type__in=[PricingRule.TYPE_VA, PricingRule.TYPE_VA_PERMANENCIER]) \
            .first()  # premier produit non VA pour l'évènement permanencier
        billet = Billet.objects.create(order=order, product=produit_non_va)
        Participant.objects.create(first_name=form.data['prenom'],
                                       last_name=form.data['nom'],
                                       email=form.data['email'],
                                       phone=form.data['phone'],
                                       billet=billet)
        assert order.is_valid(), "La commande n'est pas valide: les quotas sont probablement dépassés"
        transaction = Transaction.objects.create(order=order, amount=order.amount,
                                                 permanence=Permanence.from_user(self.request.user),
                                                 moyen=PaymentMethod.objects.get(id=form.data['moyen'])
                                                 )
        order.status = Order.STATUS_VALIDATED
        order.save()
        order.send_tickets()
        return render(self.request, self.template_name, context={
            'client': client,
            'billet': billet,
            'order': order,
            'transaction': transaction,
            'form': self.form_class(),
            'produit':billet.product
        })

class QuickAchatVAView(PermanencierMixin, FormView):
    form_class = QuickAchatVAForm
    template_name = 'permanence/quick-va.html'
    api = AdhesionAPI()

    @method_decorator(transaction.atomic)  # pour éviter de niquer trop la DB si y'a une erreur
    def form_valid(self, form):
        try:
            member = self.api.get_member_from_card(form.data['code'])
        except ValueError:
            return display_error(self.request,"Carte VA invalide", '.')
        try:
            client = Client.objects.get(email=member['email'])
            client.numero_VA = form.data['code']
            client.id_adhesion = member['id']
            client.save()
        except Client.DoesNotExist:
            try:
                user = User.objects.get(email=member['email'])
            except User.DoesNotExist:
                user = User.objects.create(first_name=member['first_name'],
                                           last_name=member['last_name'],
                                           email=member['email'],
                                           username=member['email']
                                           )
            client = Client.objects.create(first_name=member['first_name'],
                                           last_name=member['last_name'],
                                           email=member['email'],
                                           phone=member['phone'],
                                           user=user,
                                           id_adhesion = member['id'],
                                           numero_VA=form.data['code'])
        #on a un client
        order = Order.objects.create(client=client, event=self.request.user.membership.events.first(), status=Order.STATUS_PAYMENT)
        produit_va = Product.objects.filter(event=order.event, rules__type__in=[PricingRule.TYPE_VA, PricingRule.TYPE_VA_PERMANENCIER]) \
            .first()  # premier produit VA pour l'évènement permanencier
        billet = Billet.objects.create(order=order, product=produit_va)
        Participant.objects.create(first_name=member['first_name'],
                                       last_name=member['last_name'],
                                       email=member['email'],
                                       phone=member['phone'],
                                       billet=billet)
        assert order.is_valid(permanencier=True), "La commande n'est pas valide: les quotas sont probablement dépassés"
        VA.objects.create(VA=form.data['code'], event=order.event)
        transaction = Transaction.objects.create(order=order, amount=order.amount,
                                                 permanence=Permanence.from_user(self.request.user),
                                                 moyen=PaymentMethod.objects.get(id=form.data['moyen'])
                                                 )
        order.status = Order.STATUS_VALIDATED
        order.save()
        return render(self.request, self.template_name, context={
            'client': client,
            'billet': billet,
            'order': order,
            'transaction': transaction,
            'form': self.form_class(),
            'produit': billet.product
        })


class CompostageVA(PermanencierMixin, FormView):
    form_class = VAForm
    template_name = "compostage_va.html"
    api = AdhesionAPI()

    def form_valid(self, form):
        numero_VA = form.cleaned_data['code']
        try:
            client = Client.objects.get(numero_VA=numero_VA)
        except Client.DoesNotExist:
            try:
                adherent = self.api.get_card(numero_VA)
                client = Client.objects.get(id_adhesion=adherent)
            except Client.DoesNotExist:
                return display_error(self.request, "Cette carte VA n'a pas été utilisée sur la billetterie", '.')
        except MultipleObjectsReturned:
            return display_error(self.request, "Plusieurs clients sont marqués comme possédant cette carte VA", '.')

        event = self.request.user.membership.events.first()
        try:
            billet = Billet.objects.get(order__event=event, order__client=client, product__rules__type__in=(
            PricingRule.TYPE_VA, PricingRule.TYPE_VA_PERMANENCIER))
        except Billet.DoesNotExist:
            return display_error(self.request, "Aucun billet trouvé", '.')
        file = Permanence.from_user(self.request.user).file
        try:
            composter(billet=billet, file=file)
        except AssertionError as e:
            return display_error(self.request, str(e), '.')
        return render(self.request, self.template_name, context={
            'client':client,
            'billet':billet,
            'success':True,
            'file':file,
            'form':self.form_class(),
        })

    def get_context_data(self, **kwargs):
        kwargs['file'] = Permanence.from_user(self.request.user).file
        return super().get_context_data(**kwargs)