from django.apps import AppConfig


class PermanencierConfig(AppConfig):
    name = 'permanencier'
