from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from typing import Dict
from api.models import Membership, Permanence

class PermissionPermanencierMixin(PermissionRequiredMixin):
    """
        Limite l'accès aux gestionnaires d'évènements
        """

    def has_permission(self):
        try:
            if self.request.user.membership.permission_level != Membership.LEVEL_VIEWER:
                return True
            else:
                return False
        except:
            return False
    def get_context_data(self, **kwargs):
        kwargs['permanence'] = Permanence.from_user(self.request.user)
        return super().get_context_data(**kwargs) #pycharm est  pas content mais ça marche vraiment

class PermanencierMixin(PermissionPermanencierMixin):
    """
    requiert l'utilisation d'une permanence ouverte
    """
    def get_context_data(self, **kwargs):
        kwargs = add_permanencier_context(self.request, kwargs)
        return super().get_context_data(**kwargs) #pycharm est  pas content mais ça marche vraiment

def permission_permanencier(function):
    def wrap(request, *args, **kwargs):
        try:
            if request.user.membership.permission_level != Membership.LEVEL_VIEWER:
                return function(request, *args, **kwargs)
            else:
                raise PermissionDenied
        except Membership.DoesNotExist:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

def add_permanencier_context(request,context: Dict):
    permanence_required = True
    if request.method == 'GET' and request.user.membership.permission_level == Membership.LEVEL_ADMIN:
        permanence_required=False
    context.update({
        'permanence': Permanence.from_user(request.user),
        'open_permanence_required':permanence_required,
    })
    return context

def permission_orga(function):
    def wrap(request, *args, **kwargs):
        try:
            if request.user.membership.permission_level in [Membership.LEVEL_ADMIN, Membership.LEVEL_MANAGER]:
                return function(request, *args, **kwargs)
            else:
                raise PermissionDenied
        except Membership.DoesNotExist:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap