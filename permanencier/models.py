from django.db import models
from api import models as api_models
from django.contrib.auth.models import User
# Create your models here.

class Order(api_models.Order):
    """
    J'utilise des classes proxy pour éviter de rajouter 1000 méthodes/property aux modèles juste pour le rendu dans les templates
    """
    class Meta:
        proxy = True

class Billet(api_models.Billet):
    class Meta:
        proxy = True

class Participant(api_models.Participant):
    class Meta:
        proxy = True

class Client(api_models.Client):
    class Meta:
        proxy = True
    def make_participant(self) -> Participant:
        return Participant(first_name=self.first_name, last_name=self.last_name, email=self.email, phone=self.phone)
    def make_user(self) -> User:
        return User(first_name=self.first_name, last_name=self.last_name, email=self.email, username=self.email)

class Product(api_models.Product):
    class Meta:
        proxy = True

class TransactionPermanencier(api_models.Transaction):
    class Meta:
        proxy = True

class Event(api_models.Event):
    class Meta:
        proxy = True