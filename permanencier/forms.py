from django import forms

from api.models import Permanence, PaymentMethod
from permanencier.models import *

class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        exclude = ('user','numero_VA', 'id_adhesion')

class ClientSearchForm(forms.Form):
    email = forms.EmailField(required=False)
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)

class ParticipantForm(forms.ModelForm):
    class Meta:
        model=Participant
        exclude = ('billet',)

class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ('transaction', 'status', 'client', 'coupon', 'event')

class BilletForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #self.fields["options"].required = False
        self.fields["product"].required = True
    class Meta:
        model = Billet
        exclude = ('canceled', 'refunded', 'order', 'options')

class TransactionPermanencierForm(forms.ModelForm):
    class Meta:
        model = TransactionPermanencier
        exclude = ('order','permanence', 'amount')

class PermanenceOpenForm(forms.ModelForm):
    class Meta:
        model= Permanence
        exclude = ('created_at', 'closed_at', 'permanencier')

class VAForm(forms.Form):
    code = forms.CharField(help_text='de la forme cXXXXXXXXXXXX')

class AdhesionMemberForm(forms.Form):
    search = forms.CharField(required=True, label="Recherche")
    gender = forms.ChoiceField(choices=(('M', 'Homme'), ('W', 'Femme'), ('', '-----')), required=False, label="Sexe", initial='')
    birthday = forms.DateField(required=False, label="Date de naissance")

class QuickAchatForm(forms.Form):
    prenom = forms.CharField(required=True, label="Prénom")
    nom = forms.CharField(required=True, label="Nom")
    email = forms.EmailField(required=True, label="Email")
    phone = forms.CharField(required=False, label="Numéro de téléphone")
    moyen = forms.ModelChoiceField(label="Moyen de paiement", queryset=PaymentMethod.objects.all())

class QuickAchatVAForm(VAForm):
    moyen = forms.ModelChoiceField(label="Moyen de paiement", queryset=PaymentMethod.objects.all())
