from django.conf.urls import url
from django.db import transaction
from django.shortcuts import render

from permanencier import views
urlpatterns = [
    url(r'error$', views.ErrorView.as_view(), name='error-view'),
    url(r'client/create/$', views.ClientCreateView.as_view(), name='client-create'),
    url(r'client/edit/(?P<pk>[0-9]+)/$', views.ClientChangeView.as_view(), name='client-edit'),
    url(r'client/search_va/$', views.AdherentSearchView.as_view(), name='adherent-search'),
    url(r'client/search/$', views.ClientSearchView.as_view(), name='client-search'),
    url(r'client/from_va/$', views.client_from_va, name='client-va'),
    url(r'client/$', views.ClientListView.as_view(), name='client-list'),
    url(r'order/create/(?P<client_id>[0-9]+)/$', views.OrderCreateView.as_view(), name='order-create'),
    url(r'order/orphans/$', views.CommandesOrphelinesListView.as_view(), name='order-orphans'),
    url(r'order/(?P<pk>[0-9]+)/view/$', views.OrderView.as_view(), name='order-view'),
    url(r'order/(?P<pk>[0-9]+)/delete/$', views.delete_order, name='order-delete'),
    url(r'order/(?P<order_id>[0-9]+)/billet/create/$', views.BilletCreateView.as_view(), name='billet-create'),
    url(r'order/(?P<order_id>[0-9]+)/billet/(?P<pk>[0-9]+)/edit/$', views.BilletChangeView.as_view(), name='billet-edit'),
    url(r'order/(?P<order_id>[0-9]+)/billet/(?P<billet_id>[0-9]+)/delete/$', views.delete_billet, name='billet-delete'),
    url(r'order/(?P<order_id>[0-9]+)/billet/(?P<billet_id>[0-9]+)/participant/create/$', views.ParticipantCreateView.as_view(), name='participant-create'),
    url(r'order/(?P<order_id>[0-9]+)/billet/(?P<billet_id>[0-9]+)/participant/(?P<pk>[0-9]+)/edit/$', views.ParticipantChangeView.as_view(), name='participant-edit'),
    url(r'order/(?P<order_id>[0-9]+)/transaction/create/$', views.TransactionCreateView.as_view(), name='transaction-create'),
    url(r'order/transaction/view/$', views.TransactionView.as_view(), name='transaction-view'),
    url(r'permanence/start/$', views.PermanenceCreateView.as_view(), name='permanence-start'),
    url(r'permanence/stop/$', views.finir_ma_permanence, name='permanence-stop'),
    url(r'permanence/list/$', views.PermanencesFiniesListView.as_view(), name='permanence-list'),
    url(r'permanence/(?P<pk>[0-9]+)/view/$', views.PermanenceView.as_view(), name='permanence-view'),
    url(r'permanence/quick-nonva/$', views.QuickAchatView.as_view(), name='quick-nonva'),
    url(r'permanence/quick-va/$', views.QuickAchatVAView.as_view(), name='quick-va'),
    url(r'remboursement/(?P<old_billet_id>[0-9]+)/passation/(?P<acheteur_client_id>[0-9]+)/$', views.annuler_et_recreer_billets, name='billets-refund-recreate'),
    url(r'remboursement/select/client/(?P<pk>[0-9]+)/$', views.select_client, name='client-select'),
    url(r'remboursement/select/billet/(?P<pk>[0-9]+)/$', views.select_billet, name='billet-select'),
    url(r'remboursement/select/client/$', views.cancel_selection_client, name='client-cancel-select'),
    url(r'remboursement/select/billet/$', views.cancel_selection_billet, name='billet-cancel-select'),
    url(r'compostage/$', views.CompostageVA.as_view(), name='composate-va'),
    url(r'^$', views.HomeView.as_view(), name='home')
]
