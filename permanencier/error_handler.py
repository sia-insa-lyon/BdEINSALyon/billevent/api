import logging
import traceback
from django.shortcuts import render
from django.utils.deprecation import MiddlewareMixin

logger = logging.getLogger('permanencier')


class PermanencierException(Exception):
    message = "Exception permanencier inconnue"
    safe_url='/'
    def __init__(self, message, safe_url, *args, **kwargs):
        self.message=message
        self.safe_url=safe_url
        super().__init__(message, **kwargs)


def display_error(request, message:str, safe_url:str):
    logger.error("Le permanencier {} a rencontré l'erreur suivante:\n {}".format(request.user, message))
    return render(request, 'error.html', context={
        'msg':message,
        'next': safe_url
    })
# Mixin for compatibility with Django <1.10
class HandlePermanencierExceptionMiddleware(MiddlewareMixin):
    def process_exception(self, request, exception):
        if isinstance(exception, PermanencierException) or isinstance(exception, AssertionError):
            message = "Erreur inconnue"
            safe_url=request.META.get('HTTP_REFERER', '/')
            if isinstance(exception, PermanencierException):
                message = exception.message
                safe_url = exception.safe_url
            if isinstance(exception, AssertionError):
                message = exception
            #traceback.print_tb(exception.__traceback__) #affiche l'erreur , en fait c'est chiant
            return display_error(request, message, safe_url)
        else: return None