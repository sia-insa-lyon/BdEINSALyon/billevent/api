# Generated by Django 3.1.1 on 2021-02-28 19:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('short_name', models.CharField(max_length=5, unique=True, verbose_name='Abréviation')),
                ('name', models.CharField(max_length=255, verbose_name='Nom')),
                ('max_wave_seats', models.IntegerField(default=100, verbose_name='Nombre maximum de diplômés de ce département autorisés dans une vague')),
            ],
            options={
                'verbose_name': 'Département',
            },
        ),
        migrations.CreateModel(
            name='Wave',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField(unique=True, verbose_name='Numéro')),
                ('allowed_departments', models.ManyToManyField(related_name='allowed_waves', to='prdd.Department', verbose_name='Départements autorisés dans cette vague')),
            ],
            options={
                'verbose_name': 'Vague',
            },
        ),
        migrations.CreateModel(
            name='WaveGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField(verbose_name='Numéro')),
                ('seats', models.IntegerField(default=100, verbose_name='Nombre de places')),
                ('wave', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='groups', to='prdd.wave', verbose_name='Vague')),
            ],
            options={
                'verbose_name': 'Groupe',
            },
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.CharField(max_length=32, unique=True, verbose_name='Token invitation billevent')),
                ('first_name', models.CharField(max_length=4096, verbose_name='Prénom')),
                ('last_name', models.CharField(max_length=4096, verbose_name='Nom')),
                ('want_food_truck', models.CharField(choices=[('oui', 'oui'), ('non', 'non'), ('-', 'pas répondu')], default='-', max_length=3, verbose_name="Prévoit d'aller manger aux food trucks")),
                ('department', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='students', to='prdd.department', verbose_name='Département')),
                ('wave_group', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='students', to='prdd.wavegroup', verbose_name='Groupe')),
            ],
            options={
                'verbose_name': 'Diplômé',
            },
        ),
    ]
