from django.http import HttpResponse, HttpResponseRedirect
# Create your views here.
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import TemplateView, FormView

from prdd.forms import StudentGroupChoiceForm
from prdd.models import Student, WaveGroup, Wave


class Index(TemplateView):
    template_name = 'index.html'


class GroupChooseView(FormView):
    form_class = StudentGroupChoiceForm
    template_name = 'group_choice.html'

    def get_student(self)->Student:
        return Student.objects.get(id=self.request.session['student_id'])

    def form_valid(self, form: StudentGroupChoiceForm):
        _s = form.save(commit=False)
        student:Student = self.get_student()
        student.want_food_truck = _s.want_food_truck
        if student.wave_group is None:  # virer pour autoriser le changement d'avis
            if student.choose_group(_s.wave_group):
                return render(self.request,'group_confirm.html',{})
            else:
                return HttpResponse("nope")
        else:
            return HttpResponse("vous avez déjà fait votre choix !")

    def get_context_data(self, **kwargs):
        kwargs = super(GroupChooseView, self).get_context_data(**kwargs)
        student = self.get_student()
        wave_groups_availble = []
        wave_groups_availble_ids = []
        for w in Wave.objects.all():
            ww: Wave = w
            if ww.has_free_seats_for_department(department=student.department):
                for wg in ww.groups.all():
                    wgg: WaveGroup = wg
                    if wgg.has_free_seats:
                        wave_groups_availble.append(wgg)
                        wave_groups_availble_ids.append(wgg.id)
        kwargs['student']=student
        kwargs['wave_groups'] = wave_groups_availble
        kwargs['a']='b'
        kwargs['form'].fields['wave_group'].queryset = WaveGroup.objects.filter(id__in=wave_groups_availble_ids)
        return kwargs

def from_link(request, token):
    try:
        student = Student.objects.get(token=token)
        request.session['student_id'] = student.id
        return HttpResponseRedirect(reverse('group_choice'))
    except Student.DoesNotExist:
        return HttpResponse("lien invalide. Vous devez utiliser le lien qu'on vous a fourni")
