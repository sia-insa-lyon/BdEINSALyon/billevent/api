from django.test import TestCase

from .models import *


# Create your tests here.

class TestPRDDtest(TestCase):
    def testWave(self):
        w = Wave.objects.create(number=1)
        g1 = WaveGroup.objects.create(wave=w, number=1, seats=2)

        tc = Department.objects.create(short_name='TC', name='Télécom', max_wave_seats=1)
        gpc = Department.objects.create(short_name='GPC', name="Génie du 1er Cycle", max_wave_seats=1)

        w.allowed_departments.add(tc)

        j = Student.objects.create(first_name='jean', last_name='r', department=tc,
                                   token='00012345678998765432112345678922')

        self.assertTrue(j.choose_group(g1))
        
        f = Student.objects.create(first_name='Hadrien', last_name='Frigo', department=gpc, token='deadbeef123123')
        self.assertFalse(f.choose_group(g1))

        print(f"{w}: {w.used_seats} places occupées")
        print(f"  > {w.free_seats_for_department(tc)} places libre pour {tc}")
        print(f"{g1}: {g1.seats} places, dont {g1.free_seats} libres: {g1.has_free_seats}")

        self.assertEqual(w.free_seats_for_department(tc), 0)
        self.assertEqual(w.free_seats_for_department(gpc), 0)
        self.assertEqual(g1.free_seats, 1)
