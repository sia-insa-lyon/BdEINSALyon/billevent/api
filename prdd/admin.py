from django.contrib import admin
from django.urls import reverse
from import_export.admin import ImportExportModelAdmin
from import_export.resources import ModelResource

from . import models


# Register your models here.
@admin.register(models.Department)
class DepartAdmin(admin.ModelAdmin):
    list_display = ['short_name', 'name', 'max_wave_seats']
    list_filter=['allowed_waves']


@admin.register(models.Wave)
class WaveAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'number', 'used_seats']
    list_filter=['allowed_departments']


@admin.register(models.WaveGroup)
class WaveGroupAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'wave', 'number', 'occupied_seats']
    list_filter = ['wave']


class StudentResource(ModelResource):
    class Meta:
        model = models.Student
        exclude = ['wave_group','want_food_truck']


def reset_choices(modeladmin, request, queryset):
    #for _s in queryset:
    #    student:models.Student = _s
    #    student.want_food_truck="-"
    #    student.wave_group=None
    #    student.save()
    #return True
    queryset.update(wave_group=None,want_food_truck='-')
reset_choices.short_description = "Remettre à zéro les choix"

@admin.register(models.Student)
class StudentAdmin(ImportExportModelAdmin):
    list_display = ('token', 'first_name', 'last_name', 'department', 'wave_group')
    list_filter = ('wave_group', 'department','want_food_truck')
    search_fields = ('first_name', 'last_name', 'token')

    resource_class = StudentResource

    actions=[reset_choices]

    def view_on_site(self, obj: models.Student):
        return reverse('token', kwargs={'token': obj.token})
